CHART_DIR_PATH_INIT="charts/init"
CHART_DIR_PATH_ARGOCD="charts/argocd"
ARGO_NAMESPACE="argocd"
CHART_DIR_PATH_CNI="charts/cilium"
CNI_NAMESPACE="cilium"
CNI_NAME="cilium"
ACD=argo-cd
REGISTRY="registry.gitlab.com"
FORWARD_PORT="8021"
MINIKUBE_KUBE_VERSION=1.30.0
REGCRED_NAME="regcred"
DEEPCYPHER_NAMESPACE=deepcypher
# additional args for setting podman auth.json config
# https://docs.podman.io/en/latest/markdown/podman-login.1.html
DOCKER_AUTH_FILE="${HOME}/.docker/config.json"
REGISTRY_AUTH_FILE="${HOME}/.podman/auth.json"
# this will override the charts values.yaml to let the chart know it should either run in "cloud" or "metal" mode.
# The only difference should be storage and loadbalancing since on bare-metal we have to do it ourselves.
KUBE_TYPE=metal
INFRA_GIT_REVISION=$(shell git rev-parse --abbrev-ref HEAD)

# Cloudflared settings
CLUSTER_NAME=arbiter
CLOUDFLARED_TUNNEL_CRED_FILE="${HOME}/.cloudflared/${CLUSTER_NAME}.yaml"

.PHONY: help
help: ## display this auto generated help message
	@echo "Please provide a make target:"
	@grep -F -h "##" $(MAKEFILE_LIST) | grep -F -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

.PHONY: all
all: minikube install forward ## create a basic minikube cluster to launch the stack onto

.PHONY: install
install: #login.lock ## bootstrap cluster into default kubeconfig location
	#helm repo add argocd https://argoproj.github.io/argo-helm/
	helm dependency update ${CHART_DIR_PATH_INIT}
	#kubectl create namespace ${DEEPCYPHER_NAMESPACE} && kubectl apply -f login.creds
	helm dependency build ${CHART_DIR_PATH_ARGOCD}
	helm upgrade --install --create-namespace --namespace ${ARGO_NAMESPACE} argo-cd ${CHART_DIR_PATH_ARGOCD}
	helm dependency build ${CHART_DIR_PATH_INIT}
	helm upgrade --install --create-namespace --namespace ${ARGO_NAMESPACE} --set="type=${KUBE_TYPE}" init ${CHART_DIR_PATH_INIT}

.PHONY: init
init:
	helm upgrade --install --create-namespace --namespace ${ARGO_NAMESPACE} --set="type=${KUBE_TYPE}" --set="global.revision=${INFRA_GIT_REVISION}" init ${CHART_DIR_PATH_INIT}

.PHONY: argocd
argocd:
	helm repo add argocd https://argoproj.github.io/argo-helm/
	helm repo update argocd
	helm dependency update ${CHART_DIR_PATH_ARGOCD}
	helm dependency build ${CHART_DIR_PATH_ARGOCD}
	helm upgrade --install --create-namespace --namespace ${ARGO_NAMESPACE} argo-cd ${CHART_DIR_PATH_ARGOCD}

.PHONY: cni
cni:
	@echo "Dont forget to patch the spec.kubeletVolumePluginPath -> None in the default Installation CR"
	helm dependency build ${CHART_DIR_PATH_CNI}
	helm upgrade --install --create-namespace --namespace ${CNI_NAMESPACE} ${CNI_NAME} ${CHART_DIR_PATH_CNI}

.PHONY: get-gpu-nodes
get-gpu-nodes:
	@# https://stackoverflow.com/questions/62264658/filter-kubectl-get-based-on-annotation
	@kubectl get nodes -o jsonpath='{.items[?(@.metadata.annotations.nfd\.node\.kubernetes\.io/extended-resources=="nvidia.com/gpu")].metadata.name}'

.PHONY: rook-tools
rook-tools:
	kubectl -n rook-ceph exec -it $(shell kubectl -n rook-ceph get pod -l "app=rook-ceph-tools" -o jsonpath='{.items[*].metadata.name}') -- ceph status
	kubectl -n rook-ceph exec -it $(shell kubectl -n rook-ceph get pod -l "app=rook-ceph-tools" -o jsonpath='{.items[*].metadata.name}') -- bash

.PHONY: rook-scrub
rook-scrub:
	kubectl -n rook-ceph exec -it $(shell kubectl -n rook-ceph get pod -l "app=rook-ceph-tools" -o jsonpath='{.items[*].metadata.name}') -- bash -c "ceph health detail | grep "not scrubbed since" | awk '{ print $2 }' | xargs -n1 ceph pg scrub"

.PHONY: forward
forward: ## forward local port to argocd server deployment for secure preview
	kubectl wait --timeout=600s --for=condition=Available=True -n ${ARGO_NAMESPACE} deployment ${ACD}-argocd-server
	kubectl -n ${ARGO_NAMESPACE} get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d && echo
	xdg-open "https://localhost:${FORWARD_PORT}?username=admin?password=" &
	kubectl port-forward svc/${ACD}-argocd-server -n ${ARGO_NAMESPACE} ${FORWARD_PORT}:443

.PHONY: proxy
proxy: ## creare some minikube proxy and a socat forward to proxy traffic over 443
	minikube -n ingress service ingress-ingress-nginx-controller --url
	# please set auth.deepcypher.me, deepcypher.me, docs.deepcypher.me api.deepcypher.me to the TCP ip redirected after this message
	sudo socat TCP-LISTEN:443,fork TCP:192.168.49.2:30443

.PHONY: uninstall
uninstall: ## uninstall this bootstrap but this will not remove argocd as it needs to reconcile
	helm uninstall --namespace ${ARGO_NAMESPACE} init

.PHONY: upgrade
upgrade: install ## upgrade both argocd and the bootstrap

.PHONY: login
login: login.lock

login.lock:
	podman login ${REGISTRY}
	kubectl create -n ${DEEPCYPHER_NAMESPACE} secret generic ${REGCRED_NAME} --from-file=.dockerconfigjson=${REGISTRY_AUTH_FILE} --type=kubernetes.io/dockerconfigjson --dry-run=client -o yaml > login.creds
	touch login.lock

.PHONY: patch-regcred
patch-regcred: login.lock
	kubectl apply -f login.creds

.PHONY: minikube
minikube: ## create a minikube cluster by first deleting minikube then creating a new one with calico etc
	minikube delete
	minikube start --cni calico --driver=podman --kubernetes-version=${MINIKUBE_KUBE_VERSION}
	# minikube addons enable ingress
	# OR forward URL and edit /etc/hosts to point to 127.0.0.1
	# minikube -n ingress service ingress-ingress-nginx-controller --url
	# sudo socat TCP-LISTEN:443,fork TCP:<URL>:30443

# THIS IS VERY IMPORTANT PLEASE FOR THE LOVE OF GOD BACK THIS UP
# You need this master key to be able to decrypt your secrets in the cluster in future
master.key:
	kubectl -n sealed-secrets get secret -l sealedsecrets.bitnami.com/sealed-secrets-key -o yaml > master.key

.PHONY: stuck
stuck: ## find stuck resources in argocd namespace
	kubectl api-resources --verbs=list --namespaced -o name | xargs -n 1 kubectl get --show-kind --ignore-not-found -n ${ARGO_NAMESPACE}

.PHONY: argotunnel
argotunnel: master.key ## Generate encrypted cloudflared argo tunnel for this cluster
	cloudflared tunnel create metal --output yaml --credentials-file ${CLOUDFLARED_TUNNEL_CRED_FILE}
	kubectl create secret generic tunnel-credentials --from-file=credentials.json=${CLOUDFLARED_TUNNEL_CRED_FILE} --dry-run=client -o yaml > ${CLUSTER_NAME}.cloudflared.yaml
