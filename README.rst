DeepCypher Kubernetes Cluster
=============================

Declarative infrastructure for argo-cd

See:

https://argo-cd.readthedocs.io/en/stable/operator-manual/cluster-bootstrapping/#helm-example

https://github.com/argoproj/argocd-example-apps/tree/master/apps
