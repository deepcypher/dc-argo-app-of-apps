#!/usr/bin/env bash
# NOTE: This uses  https://kislyuk.github.io/yq/ in go not python yq
# this bash script will search every directory under charts/ looking for .sealed.yaml files
# it will then use the name and namespace to fetch the unsealed secret from the cluster.
# it will then output the unsealed secret to .unsealed.yaml beside each original sealed file.
# it will then base64 every value in the map under the .data key and output into the same value in .unsealed.
# each decoded secret will be in the same key-value as the original.
# it will then find all .data keys and turn them into .stringData since they are now decoded for kube.

for file in $(find charts/ -name '*.sealed.yaml'); do
	echo "finding  - $file"
	namespace=$(yq -r '.metadata.namespace' $file)
	name=$(yq -r '.metadata.name' $file)
	echo "fetching - $namespace/$name"
	outfile=$(sed -e 's/.sealed.yaml/.unsealed.yaml/' <<< "$file")
	kubectl get secret -n $namespace $name -o yaml > $outfile
	echo "decoding - $outfile"
	decoded=$(yq --yaml-output '.data |= map_values(@base64d)' $outfile)
	# if decoded is not empty or unset
	if [ ! -z "$decoded" ]; then
		echo "$decoded" > $outfile
		# remove unwanted fields
		yq -yi 'del(.metadata.ownerReferences) | del(.metadata.resourceVersion) | del(.metadata.uid) | del(.metadata.creationTimestamp)' $outfile
		# mark key values as stringData as they are no longer base64
		sed -e 's/^data:/stringData:/' -i $outfile
	fi
	echo
done
