#!/usr/bin/env bash
# NOTE: This uses  https://kislyuk.github.io/yq/ in go not python yq
# this bash script will search every directory under charts/ looking for .unsealed.yaml files
# it will kubeseal the secret and output it to .sealed.yaml beside each original unsealed file.
# this is the equivelant of running the following command for each sealed file:
# cat charts/ghost/templates/backup/ghost-backup.unsealed.yaml | kubeseal --cert ~/git/fc-helm/public.cert -o yaml > charts/ghost/templates/backup/ghost-backup.sealed.yaml

# check some argument given
if [[ $# -eq 0 ]] ; then
  echo 'No arguments supplied expected cert file path'
  exit 1
fi

CERT=$1

# check cert exists
if [ ! -f "$CERT" ]; then
	echo "cert file $CERT does not exist"
	exit 1
fi

echo "searching for .unsealed.yaml file in ${PWD}/charts/"
echo

for file in $(find charts/ -name '*.unsealed.yaml'); do
	echo "finding - $file"
	# check file is not empty
	if [ -s "$file" ]; then
		echo "sealing - $file"
		outfile=$(sed -e 's/.unsealed.yaml/.sealed.yaml/' <<< "$file")
		cat $file | kubeseal --cert $CERT -o yaml > $outfile
		echo "sealed  - $outfile"
	else
		echo "WARNING: no content in $file. Skipping."
	fi
	echo
done
