#!/usr/bin/env bash
# https://rook.io/docs/rook/latest/Storage-Configuration/Monitoring/ceph-dashboard/#enable-the-ceph-dashboard
FORWARD_PORT=8443
echo "admin"
kubectl -n rook-ceph get secret rook-ceph-dashboard-password -o jsonpath="{['data']['password']}" | base64 --decode && echo
xdg-open "https://localhost:${FORWARD_PORT}?username=admin?password=" &
kubectl port-forward svc/rook-ceph-mgr-dashboard -n rook-ceph ${FORWARD_PORT}:https-dashboard
