BSL_NAME=$1
VSL_NAME=$2

echo "Determine which secret and key the BackupStorageLocation '$BSL_NAME' is using"
BSL_SECRET=$(kubectl get backupstoragelocations.velero.io -n velero $BSL_NAME -o yaml -o jsonpath={.spec.credential.name})
BSL_SECRET_KEY=$(kubectl get backupstoragelocations.velero.io -n velero $BSL_NAME -o yaml -o jsonpath={.spec.credential.key})
echo

echo  "Confirm that the '$BSL_NAME' (BSL) secret '$BSL_SECRET' exists"
kubectl -n velero get secret $BSL_SECRET
echo

echo "Print the content of the '$BSL_NAME' secret and ensure it is correct"
kubectl -n velero get secret $BSL_SECRET -ojsonpath={.data.$BSL_SECRET_KEY} | base64 --decode
echo

echo "Determine which secret and key the VolumeSnapshotLocation '$VSL_NAME' is using"
VSL_SECRET=$(kubectl get volumesnapshotlocations.velero.io -n velero $VSL_NAME -o yaml -o jsonpath={.spec.credential.name})
VSL_SECRET_KEY=$(kubectl get volumesnapshotlocations.velero.io -n velero $VSL_NAME -o yaml -o jsonpath={.spec.credential.key})
echo

echo "Confirm that the secret '$VSL_SECRET' exists"
kubectl -n velero get secret $VSL_SECRET
echo

echo "Print the content of the secret '$VSL_SECRET' and ensure it is correct"
kubectl -n velero get secret $VSL_SECRET -ojsonpath={.data.$VSL_SECRET_KEY} | base64 --decode
