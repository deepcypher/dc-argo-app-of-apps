# Nextcloud Data Side-Loading

This directory deals with side-loading nextcloud pvcs with data for automatic binding.
This is particularly helpful during migrations where data may be on a completely seperate system.

The plan:

- Create PVCs which mimic nextclouds PVCs but have the retain option set so they do not get deleted during the migration.
- Create a PVC loader pod which will take data from us and insert it into the PVCs before nextcloud is running. (Use the krsync.sh script to do this)
	```bash
		sudo bash PATH/TO/krsync.sh -av --progress --stats PATH/TO/nextcloud-mariadb-0/. pvloader@nextcloud:/data/nextcloud-mariadb-0/.
		sudo bash PATH/TO/krsync.sh -av --progress --stats PATH/TO/nextcloud-main-0/. pvloader@nextcloud:/data/nextcloud-main-0/.
	```
- Remove the PVC loader after we have completed our data loading.
- Start nextcloud and ensure that it attempts to bind to our PVCs without permanent configuration of nextcloud necessary.
