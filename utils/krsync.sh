#!/usr/bin/env bash
# https://serverfault.com/a/887402
# put the following into a file called rsync-helper.sh:
# pod=$1;shift;kubectl exec -i $pod -- "$@"
# then call it like this:
# rsync -av --progress --stats -e './rsync-helper.sh' source-dir/ thePodName:/tmp/dest-dir
# OR use this script:

if [ -z "$KRSYNC_STARTED" ]; then
    export KRSYNC_STARTED=true
    exec rsync --blocking-io -a --rsh "$0" $@
fi

# Running as --rsh
namespace=''
pod=$1
shift

# If user uses pod@namespace, rsync passes args as: {us} -l pod namespace ...
if [ "X$pod" = "X-l" ]; then
    pod=$1
    shift
    namespace="-n $1"
    shift
fi

exec kubectl $namespace exec -i $pod -- "$@"
