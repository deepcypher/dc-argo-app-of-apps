# dc-template

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.1.0](https://img.shields.io/badge/AppVersion-0.1.0-informational?style=flat-square)

DeepCypher AoA template cluster decleration

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| exmongo.persistence.size | string | `"9Gi"` |  |
| exmongo.version | string | `"10.30.12"` |  |

