# dex

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.dexidp.io | dex | 0.20.0 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| dex.config.connectors[0].claimMapping | string | `nil` |  |
| dex.config.connectors[0].clientID | string | `"$AUTH_CLIENT_ID"` |  |
| dex.config.connectors[0].clientSecret | string | `"$AUTH_CLIENT_SECRET"` |  |
| dex.config.connectors[0].config.issuer | string | `"https://auth.deepcypher.me"` |  |
| dex.config.connectors[0].id | string | `"authentik"` |  |
| dex.config.connectors[0].name | string | `"authentik"` |  |
| dex.config.connectors[0].redirectURI | string | `"https://dex.deepcypher.me/callback"` |  |
| dex.config.connectors[0].type | string | `"oidc"` |  |
| dex.config.enablePasswordDB | bool | `true` |  |
| dex.config.issuer | string | `"http://dex.deepcypher.me"` |  |
| dex.config.storage.type | string | `"memory"` |  |
| dex.enabled | bool | `true` |  |
| dex.fullnameOverride | string | `"dex"` |  |
| dex.ingress.annotations."cert-manager.io/cluster-issuer" | string | `"aux-issuer"` |  |
| dex.ingress.className | string | `"traefik"` |  |
| dex.ingress.enabled | bool | `true` |  |
| dex.ingress.hosts[0].host | string | `"dex.deepcypher.me"` |  |
| dex.ingress.hosts[0].paths[0].path | string | `"/"` |  |
| dex.ingress.hosts[0].paths[0].pathType | string | `"Prefix"` |  |
| dex.ingress.tls[0].hosts[0] | string | `"dex.deepcypher.me"` |  |
| dex.ingress.tls[0].secretName | string | `"dex-cert"` |  |

