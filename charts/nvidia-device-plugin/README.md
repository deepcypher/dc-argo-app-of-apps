# nvidia-gpu-operator

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://nvidia.github.io/k8s-device-plugin | nvidia-device-plugin | 0.17.0 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| nvidia-device-plugin.gfd.enabled | bool | `true` |  |
| nvidia-device-plugin.nfd.enabled | bool | `false` |  |
| nvidia-device-plugin.runtimeClassName | string | `"nvidia"` |  |

