# loki

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 4](https://img.shields.io/badge/AppVersion-4-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://grafana.github.io/helm-charts | loki-distributed | 0.80.1 |
| oci://registry-1.docker.io/bitnamicharts | minio | 14.8.6 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| loki-distributed.backend.replicas | int | `0` |  |
| loki-distributed.bloomCompactor.replicas | int | `0` |  |
| loki-distributed.bloomGateway.replicas | int | `0` |  |
| loki-distributed.compactor.extraArgs[0] | string | `"-config.expand-env=true"` |  |
| loki-distributed.compactor.extraArgs[1] | string | `"--log.level=debug"` |  |
| loki-distributed.compactor.extraArgs[2] | string | `"--print-config-stderr"` |  |
| loki-distributed.compactor.extraEnv[0].name | string | `"S3_ACCESS_KEY"` |  |
| loki-distributed.compactor.extraEnv[0].valueFrom.secretKeyRef.key | string | `"root-user"` |  |
| loki-distributed.compactor.extraEnv[0].valueFrom.secretKeyRef.name | string | `"minio"` |  |
| loki-distributed.compactor.extraEnv[0].valueFrom.secretKeyRef.optional | bool | `false` |  |
| loki-distributed.compactor.extraEnv[1].name | string | `"S3_SECRET_KEY"` |  |
| loki-distributed.compactor.extraEnv[1].valueFrom.secretKeyRef.key | string | `"root-password"` |  |
| loki-distributed.compactor.extraEnv[1].valueFrom.secretKeyRef.name | string | `"minio"` |  |
| loki-distributed.compactor.extraEnv[1].valueFrom.secretKeyRef.optional | bool | `false` |  |
| loki-distributed.compactor.extraEnv[2].name | string | `"S3_URL"` |  |
| loki-distributed.compactor.extraEnv[2].valueFrom.secretKeyRef.key | string | `"s3-url"` |  |
| loki-distributed.compactor.extraEnv[2].valueFrom.secretKeyRef.name | string | `"minio"` |  |
| loki-distributed.compactor.extraEnv[2].valueFrom.secretKeyRef.optional | bool | `false` |  |
| loki-distributed.compactor.maxUnavailable | int | `1` |  |
| loki-distributed.compactor.replicas | int | `1` |  |
| loki-distributed.deploymentMode | string | `"Distributed"` |  |
| loki-distributed.distributor.extraArgs[0] | string | `"-config.expand-env=true"` |  |
| loki-distributed.distributor.extraArgs[1] | string | `"--log.level=debug"` |  |
| loki-distributed.distributor.extraArgs[2] | string | `"--print-config-stderr"` |  |
| loki-distributed.distributor.extraEnv[0].name | string | `"S3_ACCESS_KEY"` |  |
| loki-distributed.distributor.extraEnv[0].valueFrom.secretKeyRef.key | string | `"root-user"` |  |
| loki-distributed.distributor.extraEnv[0].valueFrom.secretKeyRef.name | string | `"minio"` |  |
| loki-distributed.distributor.extraEnv[0].valueFrom.secretKeyRef.optional | bool | `false` |  |
| loki-distributed.distributor.extraEnv[1].name | string | `"S3_SECRET_KEY"` |  |
| loki-distributed.distributor.extraEnv[1].valueFrom.secretKeyRef.key | string | `"root-password"` |  |
| loki-distributed.distributor.extraEnv[1].valueFrom.secretKeyRef.name | string | `"minio"` |  |
| loki-distributed.distributor.extraEnv[1].valueFrom.secretKeyRef.optional | bool | `false` |  |
| loki-distributed.distributor.extraEnv[2].name | string | `"S3_URL"` |  |
| loki-distributed.distributor.extraEnv[2].valueFrom.secretKeyRef.key | string | `"s3-url"` |  |
| loki-distributed.distributor.extraEnv[2].valueFrom.secretKeyRef.name | string | `"minio"` |  |
| loki-distributed.distributor.extraEnv[2].valueFrom.secretKeyRef.optional | bool | `false` |  |
| loki-distributed.distributor.maxUnavailable | int | `1` |  |
| loki-distributed.distributor.replicas | int | `2` |  |
| loki-distributed.fullnameOverride | string | `"loki"` |  |
| loki-distributed.indexGateway.extraArgs[0] | string | `"-config.expand-env=true"` |  |
| loki-distributed.indexGateway.extraArgs[1] | string | `"--log.level=debug"` |  |
| loki-distributed.indexGateway.extraArgs[2] | string | `"--print-config-stderr"` |  |
| loki-distributed.indexGateway.extraEnv[0].name | string | `"S3_ACCESS_KEY"` |  |
| loki-distributed.indexGateway.extraEnv[0].valueFrom.secretKeyRef.key | string | `"root-user"` |  |
| loki-distributed.indexGateway.extraEnv[0].valueFrom.secretKeyRef.name | string | `"minio"` |  |
| loki-distributed.indexGateway.extraEnv[0].valueFrom.secretKeyRef.optional | bool | `false` |  |
| loki-distributed.indexGateway.extraEnv[1].name | string | `"S3_SECRET_KEY"` |  |
| loki-distributed.indexGateway.extraEnv[1].valueFrom.secretKeyRef.key | string | `"root-password"` |  |
| loki-distributed.indexGateway.extraEnv[1].valueFrom.secretKeyRef.name | string | `"minio"` |  |
| loki-distributed.indexGateway.extraEnv[1].valueFrom.secretKeyRef.optional | bool | `false` |  |
| loki-distributed.indexGateway.extraEnv[2].name | string | `"S3_URL"` |  |
| loki-distributed.indexGateway.extraEnv[2].valueFrom.secretKeyRef.key | string | `"s3-url"` |  |
| loki-distributed.indexGateway.extraEnv[2].valueFrom.secretKeyRef.name | string | `"minio"` |  |
| loki-distributed.indexGateway.extraEnv[2].valueFrom.secretKeyRef.optional | bool | `false` |  |
| loki-distributed.indexGateway.maxUnavailable | int | `1` |  |
| loki-distributed.indexGateway.replicas | int | `2` |  |
| loki-distributed.ingester.extraArgs[0] | string | `"-config.expand-env=true"` |  |
| loki-distributed.ingester.extraArgs[1] | string | `"--log.level=debug"` |  |
| loki-distributed.ingester.extraArgs[2] | string | `"--print-config-stderr"` |  |
| loki-distributed.ingester.extraEnv[0].name | string | `"S3_ACCESS_KEY"` |  |
| loki-distributed.ingester.extraEnv[0].valueFrom.secretKeyRef.key | string | `"root-user"` |  |
| loki-distributed.ingester.extraEnv[0].valueFrom.secretKeyRef.name | string | `"minio"` |  |
| loki-distributed.ingester.extraEnv[0].valueFrom.secretKeyRef.optional | bool | `false` |  |
| loki-distributed.ingester.extraEnv[1].name | string | `"S3_SECRET_KEY"` |  |
| loki-distributed.ingester.extraEnv[1].valueFrom.secretKeyRef.key | string | `"root-password"` |  |
| loki-distributed.ingester.extraEnv[1].valueFrom.secretKeyRef.name | string | `"minio"` |  |
| loki-distributed.ingester.extraEnv[1].valueFrom.secretKeyRef.optional | bool | `false` |  |
| loki-distributed.ingester.extraEnv[2].name | string | `"S3_URL"` |  |
| loki-distributed.ingester.extraEnv[2].valueFrom.secretKeyRef.key | string | `"s3-url"` |  |
| loki-distributed.ingester.extraEnv[2].valueFrom.secretKeyRef.name | string | `"minio"` |  |
| loki-distributed.ingester.extraEnv[2].valueFrom.secretKeyRef.optional | bool | `false` |  |
| loki-distributed.ingester.maxUnavailable | int | `1` |  |
| loki-distributed.ingester.replicas | int | `2` |  |
| loki-distributed.loki.ingester.chunk_encoding | string | `"snappy"` |  |
| loki-distributed.loki.querier.max_concurrent | int | `4` |  |
| loki-distributed.loki.schemaConfig.configs[0].from | string | `"2024-04-01"` |  |
| loki-distributed.loki.schemaConfig.configs[0].index.period | string | `"24h"` |  |
| loki-distributed.loki.schemaConfig.configs[0].index.prefix | string | `"loki_index_"` |  |
| loki-distributed.loki.schemaConfig.configs[0].object_store | string | `"aws"` |  |
| loki-distributed.loki.schemaConfig.configs[0].schema | string | `"v13"` |  |
| loki-distributed.loki.schemaConfig.configs[0].store | string | `"boltdb-shipper"` |  |
| loki-distributed.loki.structuredConfig.storage_config.aws.insecure | bool | `true` |  |
| loki-distributed.loki.structuredConfig.storage_config.aws.s3 | string | `"${S3_URL}"` |  |
| loki-distributed.loki.structuredConfig.storage_config.aws.s3forcepathstyle | bool | `true` |  |
| loki-distributed.loki.structuredConfig.storage_config.boltdb_shipper.shared_store | string | `"s3"` |  |
| loki-distributed.loki.tracing.enabled | bool | `true` |  |
| loki-distributed.minio.enabled | bool | `false` |  |
| loki-distributed.querier.extraArgs[0] | string | `"-config.expand-env=true"` |  |
| loki-distributed.querier.extraArgs[1] | string | `"--log.level=debug"` |  |
| loki-distributed.querier.extraArgs[2] | string | `"--print-config-stderr"` |  |
| loki-distributed.querier.extraEnv[0].name | string | `"S3_ACCESS_KEY"` |  |
| loki-distributed.querier.extraEnv[0].valueFrom.secretKeyRef.key | string | `"root-user"` |  |
| loki-distributed.querier.extraEnv[0].valueFrom.secretKeyRef.name | string | `"minio"` |  |
| loki-distributed.querier.extraEnv[0].valueFrom.secretKeyRef.optional | bool | `false` |  |
| loki-distributed.querier.extraEnv[1].name | string | `"S3_SECRET_KEY"` |  |
| loki-distributed.querier.extraEnv[1].valueFrom.secretKeyRef.key | string | `"root-password"` |  |
| loki-distributed.querier.extraEnv[1].valueFrom.secretKeyRef.name | string | `"minio"` |  |
| loki-distributed.querier.extraEnv[1].valueFrom.secretKeyRef.optional | bool | `false` |  |
| loki-distributed.querier.extraEnv[2].name | string | `"S3_URL"` |  |
| loki-distributed.querier.extraEnv[2].valueFrom.secretKeyRef.key | string | `"s3-url"` |  |
| loki-distributed.querier.extraEnv[2].valueFrom.secretKeyRef.name | string | `"minio"` |  |
| loki-distributed.querier.extraEnv[2].valueFrom.secretKeyRef.optional | bool | `false` |  |
| loki-distributed.querier.maxUnavailable | int | `1` |  |
| loki-distributed.querier.replicas | int | `2` |  |
| loki-distributed.queryFrontend.extraArgs[0] | string | `"-config.expand-env=true"` |  |
| loki-distributed.queryFrontend.extraArgs[1] | string | `"--log.level=debug"` |  |
| loki-distributed.queryFrontend.extraArgs[2] | string | `"--print-config-stderr"` |  |
| loki-distributed.queryFrontend.extraEnv[0].name | string | `"S3_ACCESS_KEY"` |  |
| loki-distributed.queryFrontend.extraEnv[0].valueFrom.secretKeyRef.key | string | `"root-user"` |  |
| loki-distributed.queryFrontend.extraEnv[0].valueFrom.secretKeyRef.name | string | `"minio"` |  |
| loki-distributed.queryFrontend.extraEnv[0].valueFrom.secretKeyRef.optional | bool | `false` |  |
| loki-distributed.queryFrontend.extraEnv[1].name | string | `"S3_SECRET_KEY"` |  |
| loki-distributed.queryFrontend.extraEnv[1].valueFrom.secretKeyRef.key | string | `"root-password"` |  |
| loki-distributed.queryFrontend.extraEnv[1].valueFrom.secretKeyRef.name | string | `"minio"` |  |
| loki-distributed.queryFrontend.extraEnv[1].valueFrom.secretKeyRef.optional | bool | `false` |  |
| loki-distributed.queryFrontend.extraEnv[2].name | string | `"S3_URL"` |  |
| loki-distributed.queryFrontend.extraEnv[2].valueFrom.secretKeyRef.key | string | `"s3-url"` |  |
| loki-distributed.queryFrontend.extraEnv[2].valueFrom.secretKeyRef.name | string | `"minio"` |  |
| loki-distributed.queryFrontend.extraEnv[2].valueFrom.secretKeyRef.optional | bool | `false` |  |
| loki-distributed.queryFrontend.maxUnavailable | int | `1` |  |
| loki-distributed.queryFrontend.replicas | int | `2` |  |
| loki-distributed.queryScheduler.extraArgs[0] | string | `"-config.expand-env=true"` |  |
| loki-distributed.queryScheduler.extraArgs[1] | string | `"--log.level=debug"` |  |
| loki-distributed.queryScheduler.extraArgs[2] | string | `"--print-config-stderr"` |  |
| loki-distributed.queryScheduler.extraEnv[0].name | string | `"S3_ACCESS_KEY"` |  |
| loki-distributed.queryScheduler.extraEnv[0].valueFrom.secretKeyRef.key | string | `"root-user"` |  |
| loki-distributed.queryScheduler.extraEnv[0].valueFrom.secretKeyRef.name | string | `"minio"` |  |
| loki-distributed.queryScheduler.extraEnv[0].valueFrom.secretKeyRef.optional | bool | `false` |  |
| loki-distributed.queryScheduler.extraEnv[1].name | string | `"S3_SECRET_KEY"` |  |
| loki-distributed.queryScheduler.extraEnv[1].valueFrom.secretKeyRef.key | string | `"root-password"` |  |
| loki-distributed.queryScheduler.extraEnv[1].valueFrom.secretKeyRef.name | string | `"minio"` |  |
| loki-distributed.queryScheduler.extraEnv[1].valueFrom.secretKeyRef.optional | bool | `false` |  |
| loki-distributed.queryScheduler.extraEnv[2].name | string | `"S3_URL"` |  |
| loki-distributed.queryScheduler.extraEnv[2].valueFrom.secretKeyRef.key | string | `"s3-url"` |  |
| loki-distributed.queryScheduler.extraEnv[2].valueFrom.secretKeyRef.name | string | `"minio"` |  |
| loki-distributed.queryScheduler.extraEnv[2].valueFrom.secretKeyRef.optional | bool | `false` |  |
| loki-distributed.queryScheduler.maxUnavailable | int | `1` |  |
| loki-distributed.queryScheduler.replicas | int | `2` |  |
| loki-distributed.read.replicas | int | `0` |  |
| loki-distributed.singleBinary.replicas | int | `0` |  |
| loki-distributed.write.replicas | int | `0` |  |
| minio.auth.existingSecret | string | `"minio"` |  |
| minio.enabled | bool | `true` |  |
| minio.fullnameOverride | string | `"minio"` |  |
| minio.ingress.annotations."cert-manager.io/cluster-issuer" | string | `"aux-issuer"` |  |
| minio.ingress.annotations."ingress.kubernetes.io/proxy-body-size" | string | `"32M"` |  |
| minio.ingress.annotations."kubernetes.io/ingress.class" | string | `"traefik"` |  |
| minio.ingress.annotations."nginx.ingress.kubernetes.io/proxy-body-size" | string | `"32M"` |  |
| minio.ingress.enabled | bool | `false` |  |
| minio.ingress.hostname | string | `"lokis3.deepcypher.me"` |  |
| minio.ingress.ingressClassName | string | `"traefik"` |  |
| minio.ingress.selfSigned | bool | `true` |  |
| minio.ingress.tls | bool | `true` |  |
| minio.metrics.prometheusAuthType | string | `"public"` |  |
| minio.metrics.serviceMonitor.enabled | bool | `true` |  |
| minio.mode | string | `"distributed"` |  |
| minio.persistence.enabled | bool | `true` |  |
| minio.persistence.size | string | `"100G"` |  |
| minio.provisioning.buckets[0].name | string | `"loki"` |  |
| minio.provisioning.buckets[0].region | string | `"us-east-1"` |  |
| minio.provisioning.buckets[0].versioning | string | `"Suspended"` |  |
| minio.provisioning.buckets[0].withLock | bool | `false` |  |
| minio.provisioning.enabled | bool | `true` |  |
| minio.statefulset.replicaCount | int | `4` |  |

