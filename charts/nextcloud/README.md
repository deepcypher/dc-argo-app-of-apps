# dc-template

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.1.0](https://img.shields.io/badge/AppVersion-0.1.0-informational?style=flat-square)

DeepCypher AoA template cluster decleration

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.bitnami.com/bitnami | mariadb | 18.2.6 |
| https://gitlab.com/api/v4/projects/55284972/packages/helm/stable | mariadb-bkp(backupd) | 0.6.1 |
| https://gitlab.com/api/v4/projects/55284972/packages/helm/stable | nextcloud-bkp(backupd) | 0.6.1 |
| https://nextcloud.github.io/helm/ | nextcloud | 5.0.2 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| environment.baseDomain | string | `"deepcypher.me"` |  |
| environment.contact.email | string | `"noreply@deepcypher.me"` |  |
| environment.contact.name | string | `"George Onoufriou"` |  |
| environment.hardware | string | `"metal"` |  |
| environment.location.name | string | `"unknown"` |  |
| environment.mode | string | `"production"` |  |
| environment.name | string | `"unknown"` |  |
| environment.revision | string | `"main"` |  |
| mariadb-bkp.backup.copyMethod | string | `"Direct"` |  |
| mariadb-bkp.backup.enabled | bool | `true` |  |
| mariadb-bkp.backup.schedule | string | `"0 2 * * *"` |  |
| mariadb-bkp.existingPVC | string | `"data-nextcloud-app-mariadb-0"` |  |
| mariadb-bkp.existingSecret | string | `"mariadb-backupd-wasabi"` |  |
| mariadb-bkp.persistence.generate | bool | `false` |  |
| mariadb-bkp.persistence.size | string | `"100G"` |  |
| mariadb-bkp.restore.enabled | bool | `false` |  |
| mariadb-bkp.restore.schedule | string | `"0 3 * * *"` |  |
| mariadb.auth.database | string | `"nextcloud"` |  |
| mariadb.auth.existingSecret | string | `"mariadb-user"` |  |
| mariadb.auth.username | string | `"archer"` |  |
| mariadb.primary.persistence.accessModes[0] | string | `"ReadWriteOnce"` |  |
| mariadb.primary.persistence.enabled | bool | `true` |  |
| mariadb.primary.persistence.size | string | `"100G"` |  |
| mariadb.primary.resources.limits.memory | string | `"1536Mi"` |  |
| mariadb.primary.resources.requests.cpu | string | `"500m"` |  |
| mariadb.primary.startupProbe.enabled | bool | `true` |  |
| mariadb.primary.startupProbe.failureThreshold | int | `20` |  |
| mariadb.primary.startupProbe.initialDelaySeconds | int | `300` |  |
| mariadb.primary.startupProbe.periodSeconds | int | `20` |  |
| mariadb.primary.startupProbe.timeoutSeconds | int | `10` |  |
| nextcloud-bkp.backup.copyMethod | string | `"Direct"` |  |
| nextcloud-bkp.backup.enabled | bool | `true` |  |
| nextcloud-bkp.backup.schedule | string | `"0 2 * * *"` |  |
| nextcloud-bkp.existingPVC | string | `"nextcloud-app-nextcloud"` |  |
| nextcloud-bkp.existingSecret | string | `"nextcloud-backupd-wasabi"` |  |
| nextcloud-bkp.persistence.generate | bool | `false` |  |
| nextcloud-bkp.persistence.size | string | `"3T"` |  |
| nextcloud-bkp.restore.enabled | bool | `false` |  |
| nextcloud-bkp.restore.schedule | string | `"0 3 * * *"` |  |
| nextcloud.externalDatabase.enabled | bool | `true` |  |
| nextcloud.externalDatabase.existingSecret.passwordKey | string | `"mariadb-password"` |  |
| nextcloud.externalDatabase.existingSecret.secretName | string | `"mariadb-user"` |  |
| nextcloud.externalDatabase.existingSecret.usernameKey | string | `"mariadb-username"` |  |
| nextcloud.externalDatabase.host | string | `"nextcloud-app-mariadb.nextcloud.svc.cluster.local:3306"` |  |
| nextcloud.externalDatabase.type | string | `"mysql"` |  |
| nextcloud.hpa.enable | bool | `true` |  |
| nextcloud.hpa.maxPods | int | `5` |  |
| nextcloud.hpa.minPods | int | `3` |  |
| nextcloud.image.pullPolicy | string | `"Always"` |  |
| nextcloud.image.repository | string | `"nextcloud"` |  |
| nextcloud.image.tag | string | `"28-apache"` |  |
| nextcloud.ingress.annotations."cert-manager.io/cluster-issuer" | string | `"aux-issuer"` |  |
| nextcloud.ingress.annotations."gethomepage.dev/description" | string | `"Homelab storage."` |  |
| nextcloud.ingress.annotations."gethomepage.dev/enabled" | string | `"true"` |  |
| nextcloud.ingress.annotations."gethomepage.dev/group" | string | `"DeepCypher"` |  |
| nextcloud.ingress.annotations."gethomepage.dev/icon" | string | `"homepage.png"` |  |
| nextcloud.ingress.annotations."gethomepage.dev/name" | string | `"Nextcloud"` |  |
| nextcloud.ingress.annotations."traefik.ingress.kubernetes.io/router.middlewares" | string | `"nextcloud-headers@kubernetescrd,nextcloud-redirect@kubernetescrd,nextcloud-redirectstatus@kubernetescrd"` |  |
| nextcloud.ingress.className | string | `"traefik"` |  |
| nextcloud.ingress.enabled | bool | `true` |  |
| nextcloud.ingress.host | string | `"nextcloud.deepcypher.me"` |  |
| nextcloud.ingress.tls[0].hosts[0] | string | `"nextcloud.deepcypher.me"` |  |
| nextcloud.ingress.tls[0].secretName | string | `"nextcloud-cert"` |  |
| nextcloud.internalDatabase.enabled | bool | `false` |  |
| nextcloud.nextcloud.configs."logging.config.php" | string | `"<?php\n$CONFIG = array (\n  #'log_type' => 'errorlog',\n  'log_type' => 'file',\n  'logfile' => 'nextcloud.log',\n  'loglevel' => 1,\n  'logdateformat' => 'F d, Y H:i:s'\n  );"` |  |
| nextcloud.nextcloud.configs."proxy.config.php" | string | `"<?php\n$CONFIG = array (\n  'trusted_proxies' => array(\n    0 => '127.0.0.1',\n    1 => '172.16.0.0/14',\n  ),\n  'forwarded_for_headers' => array('HTTP_X_FORWARDED_FOR'),\n);"` |  |
| nextcloud.nextcloud.existingSecret.enabled | bool | `true` |  |
| nextcloud.nextcloud.existingSecret.passwordKey | string | `"password"` |  |
| nextcloud.nextcloud.existingSecret.secretName | string | `"nextcloud-user"` |  |
| nextcloud.nextcloud.existingSecret.usernameKey | string | `"username"` |  |
| nextcloud.nextcloud.host | string | `"nextcloud.deepcypher.me"` |  |
| nextcloud.persistence.enabled | bool | `true` |  |
| nextcloud.persistence.size | string | `"3T"` |  |
| nextcloud.resources.requests.cpu | string | `"500m"` |  |
| nextcloud.resources.requests.memory | string | `"6Gi"` |  |
| nextcloud.startupProbe.enabled | bool | `true` |  |
| nextcloud.startupProbe.failureThreshold | int | `100` |  |
| nextcloud.startupProbe.initialDelaySeconds | int | `20` |  |
| nextcloud.startupProbe.periodSeconds | int | `20` |  |
| nextcloud.startupProbe.timeoutSeconds | int | `10` |  |
| oidc.realm | string | `"deepcypher"` |  |

