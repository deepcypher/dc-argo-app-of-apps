# NextCloud

Folder containing all NextCloud components as standalone.

To rescan all users folders for updates you can:
```
sudo -u www-data php occ files:scan --all
```

In later version of nextcloud like the ones we use there is a problem with the docker image not assigning enough space to PHP. This means when we use php occ it can result in memory errors. To fix this you may need to run the occ commands like:
```
 sudo -u www-data PHP_MEMORY_LIMIT=512M php occ COMMAND
```

Certain internal links default to http and since we use a reverse proxy to handle https this sometimes leads to issues. https://help.nextcloud.com/t/cannot-grant-access/64566

It may be necessary to add:
```
'overwriteprotocol' => 'https',
```
to the end of the config / array variable in config/config.php. We have seen this effect desktop and mobile clients in particular. This is of course not a permenant fix so since so many things seem to be missing from the default image we might create our own that is fully functional.

A full command for instance to disable maintenance mode might look something like:

```
kubectl exec --stdin --tty -n nextcloud nextcloud-xxxxxxxxxx-xxxxx -- sudo -u www-data PHP_MEMORY_LIMIT=512M php occ maintenance:mode --off

```

Or if it does not come with sudo pre-installed we can instead pipe in a chain of commands like:

```
kubectl exec --stdin --tty -n nextcloud nextcloud-xxxxxxxxxx-xxxxx -- bash -c "apt update && apt install sudo && sudo -u www-data PHP_MEMORY_LIMIT=512M php occ maintenance:mode --off"
```
Make sure to replace the sequence of xs' like xxxxx with whatever the pod in question is
