Users and roles are created declaratively.

Check teleport running:

```bash
kubectl exec -it -n teleport deployments/teleport-auth -- tctl status
```

Initial reset emails can be sent with:

```bash
kubectl exec -it -n teleport deployments/teleport-auth -- tctl users reset george
```
