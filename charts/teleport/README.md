# teleport

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 4](https://img.shields.io/badge/AppVersion-4-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.releases.teleport.dev | teleport-cluster | 17.0.5 |
| https://gitlab.com/api/v4/projects/55284972/packages/helm/stable | teleport-bkp(backupd) | 0.7.0 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| environment.baseDomain | string | `"deepcypher.me"` |  |
| environment.contact.email | string | `"noreply@deepcypher.me"` |  |
| environment.contact.name | string | `"George Onoufriou"` |  |
| environment.hardware | string | `"metal"` |  |
| environment.location.name | string | `"unknown"` |  |
| environment.mode | string | `"production"` |  |
| environment.name | string | `"unknown"` |  |
| environment.revision | string | `"main"` |  |
| ingress.enabled | bool | `true` |  |
| teleport-bkp.backup.copyMethod | string | `"Direct"` |  |
| teleport-bkp.backup.enabled | bool | `false` |  |
| teleport-bkp.backup.schedule | string | `"0 2 * * *"` |  |
| teleport-bkp.existingPVC | string | `"teleport"` |  |
| teleport-bkp.existingSecret | string | `"teleport-backupd-wasabi"` |  |
| teleport-bkp.persistence.generate | bool | `false` |  |
| teleport-bkp.persistence.size | string | `"10Gi"` |  |
| teleport-bkp.restore.asOf | string | `"2024-11-28T20:00:00-00:00"` |  |
| teleport-bkp.restore.enabled | bool | `false` |  |
| teleport-bkp.restore.schedule | string | `"0 4 * * *"` |  |
| teleport-bkp.restore.trigger.manual | string | `"manual-2024-11-28T20-00-00"` |  |
| teleport-cluster.acme | bool | `false` |  |
| teleport-cluster.acmeEmail | string | `""` |  |
| teleport-cluster.clusterName | string | `"teleport.deepcypher.me"` |  |
| teleport-cluster.ingress.enabled | bool | `true` |  |
| teleport-cluster.ingress.useExisting | bool | `true` |  |
| teleport-cluster.operator.enabled | bool | `true` |  |
| teleport-cluster.persistence.enabled | bool | `true` |  |
| teleport-cluster.persistence.size | string | `"10Gi"` |  |
| teleport-cluster.podMonitor.enabled | bool | `true` |  |
| teleport-cluster.proxy.annotations.service."traefik.ingress.kubernetes.io/service.serverstransport" | string | `"teleport-insecure@kubernetescrd"` |  |
| teleport-cluster.proxyListenerMode | string | `"multiplex"` |  |
| teleport-cluster.service.type | string | `"ClusterIP"` |  |

