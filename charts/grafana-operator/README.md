# grafana-operator

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| oci://ghcr.io/grafana/helm-charts | grafana-operator | v5.15.1 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| grafana-operator.logging.encoder | string | `"console"` | Log encoding ("console", "json") |
| grafana-operator.logging.level | string | `"info"` | Configure the verbosity of logging ("debug", "error", "info") |
| grafana-operator.logging.time | string | `"rfc3339"` | Time encoding ("epoch", "iso8601", "millis", "nano", "rfc3339", "rfc3339nano") |
| grafana-operator.serviceMonitor.enabled | bool | `true` |  |
| grafana-operator.watchNamespaces | string | `"grafana"` |  |

