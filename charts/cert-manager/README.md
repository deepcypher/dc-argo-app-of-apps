# infra-cert-manager

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.1.0](https://img.shields.io/badge/AppVersion-0.1.0-informational?style=flat-square)

DeepCypher AoA template cluster decleration for cert-manager infra

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.jetstack.io | cert-manager | v1.17.1 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| cert-manager.installCRDs | bool | `true` |  |
| environment.baseDomain | string | `"deepcypher.me"` |  |
| environment.contact.email | string | `"noreply@deepcypher.me"` |  |
| environment.contact.name | string | `"George Onoufriou"` |  |
| environment.hardware | string | `"metal"` |  |
| environment.location.name | string | `"unknown"` |  |
| environment.mode | string | `"production"` |  |
| environment.name | string | `"unknown"` |  |
| environment.revision | string | `"main"` |  |
| global.cert.issuer.email | string | `"acme@deepcypher.me"` |  |
| ingress.class.name | string | `"traefik"` |  |

