# jellyfin

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://gitlab.com/api/v4/projects/55284972/packages/helm/stable | jellymeta | 0.3.0 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| jellymeta.filebrowser.enabled | bool | `true` |  |
| jellymeta.filebrowser.persistence.accessModes[0] | string | `"ReadWriteOnce"` |  |
| jellymeta.filebrowser.persistence.enabled | bool | `true` |  |
| jellymeta.filebrowser.persistence.size | string | `"8Gi"` |  |
| jellymeta.filebrowser.volumeMounts[0].mountPath | string | `"/database"` |  |
| jellymeta.filebrowser.volumeMounts[0].name | string | `"data"` |  |
| jellymeta.filebrowser.volumeMounts[0].readOnly | bool | `false` |  |
| jellymeta.filebrowser.volumeMounts[0].subPath | string | `"database"` |  |
| jellymeta.filebrowser.volumeMounts[1].mountPath | string | `"/srv"` |  |
| jellymeta.filebrowser.volumeMounts[1].name | string | `"data"` |  |
| jellymeta.filebrowser.volumeMounts[1].readOnly | bool | `false` |  |
| jellymeta.filebrowser.volumeMounts[1].subPath | string | `"files"` |  |
| jellymeta.filebrowser.volumeMounts[2].mountPath | string | `"/.filebrowser.json"` |  |
| jellymeta.filebrowser.volumeMounts[2].name | string | `"config"` |  |
| jellymeta.filebrowser.volumeMounts[2].readOnly | bool | `false` |  |
| jellymeta.filebrowser.volumeMounts[2].subPath | string | `".filebrowser.json"` |  |
| jellymeta.filebrowser.volumeMounts[3].mountPath | string | `"/srv/jellyfin"` |  |
| jellymeta.filebrowser.volumeMounts[3].name | string | `"jellyfin"` |  |
| jellymeta.filebrowser.volumeMounts[3].readOnly | bool | `false` |  |
| jellymeta.filebrowser.volumeMounts[3].subPath | string | `"media"` |  |
| jellymeta.filebrowser.volumeMounts[4].mountPath | string | `"/srv/qbit"` |  |
| jellymeta.filebrowser.volumeMounts[4].name | string | `"qbit"` |  |
| jellymeta.filebrowser.volumeMounts[4].readOnly | bool | `false` |  |
| jellymeta.filebrowser.volumeMounts[4].subPath | string | `"downloads"` |  |
| jellymeta.filebrowser.volumes[0].name | string | `"data"` |  |
| jellymeta.filebrowser.volumes[0].persistentVolumeClaim.claimName | string | `"jellyfin-filebrowser"` |  |
| jellymeta.filebrowser.volumes[1].configMap.items[0].key | string | `".filebrowser.json"` |  |
| jellymeta.filebrowser.volumes[1].configMap.items[0].path | string | `".filebrowser.json"` |  |
| jellymeta.filebrowser.volumes[1].configMap.name | string | `"filebrowser"` |  |
| jellymeta.filebrowser.volumes[1].name | string | `"config"` |  |
| jellymeta.filebrowser.volumes[2].name | string | `"jellyfin"` |  |
| jellymeta.filebrowser.volumes[2].persistentVolumeClaim.claimName | string | `"jellyfin"` |  |
| jellymeta.filebrowser.volumes[3].name | string | `"qbit"` |  |
| jellymeta.filebrowser.volumes[3].persistentVolumeClaim.claimName | string | `"qbittorrent"` |  |
| jellymeta.jellyfin.enabled | bool | `true` |  |
| jellymeta.jellyfin.ingress.annotations."cert-manager.io/cluster-issuer" | string | `"aux-issuer"` |  |
| jellymeta.jellyfin.ingress.annotations."traefik.ingress.kubernetes.io/router.middlewares" | string | `"traefik-headers@kubernetescrd"` |  |
| jellymeta.jellyfin.ingress.className | string | `""` |  |
| jellymeta.jellyfin.ingress.enabled | bool | `true` |  |
| jellymeta.jellyfin.ingress.hosts[0].host | string | `"jellyfin.deepcypher.me"` |  |
| jellymeta.jellyfin.ingress.hosts[0].paths[0].path | string | `"/"` |  |
| jellymeta.jellyfin.ingress.hosts[0].paths[0].pathType | string | `"Prefix"` |  |
| jellymeta.jellyfin.ingress.tls[0].hosts[0] | string | `"jellyfin.deepcypher.me"` |  |
| jellymeta.jellyfin.ingress.tls[0].secretName | string | `"jellyfin.deepcypher.me-tls"` |  |
| jellymeta.jellyfin.livenessProbe.failureThreshold | int | `12` |  |
| jellymeta.jellyfin.livenessProbe.httpGet.path | string | `"/"` |  |
| jellymeta.jellyfin.livenessProbe.httpGet.port | string | `"http"` |  |
| jellymeta.jellyfin.livenessProbe.periodSeconds | int | `10` |  |
| jellymeta.jellyfin.persistence.accessModes[0] | string | `"ReadWriteMany"` |  |
| jellymeta.jellyfin.persistence.enabled | bool | `true` |  |
| jellymeta.jellyfin.persistence.size | string | `"3T"` |  |
| jellymeta.jellyfin.readinessProbe.failureThreshold | int | `6` |  |
| jellymeta.jellyfin.readinessProbe.httpGet.path | string | `"/"` |  |
| jellymeta.jellyfin.readinessProbe.httpGet.port | string | `"http"` |  |
| jellymeta.jellyfin.readinessProbe.periodSeconds | int | `10` |  |
| jellymeta.jellyfin.resources.limits.memory | string | `"6G"` |  |
| jellymeta.jellyfin.resources.requests.cpu | string | `"600m"` |  |
| jellymeta.jellyfin.resources.requests.memory | string | `"6G"` |  |
| jellymeta.jellyfin.startupProbe.failureThreshold | int | `12` |  |
| jellymeta.jellyfin.startupProbe.httpGet.path | string | `"/"` |  |
| jellymeta.jellyfin.startupProbe.httpGet.port | string | `"http"` |  |
| jellymeta.jellyfin.startupProbe.initialDelaySeconds | int | `10` |  |
| jellymeta.jellyfin.startupProbe.periodSeconds | int | `10` |  |
| jellymeta.metube.enabled | bool | `false` |  |
| jellymeta.metube.persistence.accessModes[0] | string | `"ReadWriteMany"` |  |
| jellymeta.metube.persistence.enabled | bool | `true` |  |
| jellymeta.metube.persistence.size | string | `"50Gi"` |  |
| jellymeta.metube.volumeMounts[0].mountPath | string | `"/downloads"` |  |
| jellymeta.metube.volumeMounts[0].name | string | `"data"` |  |
| jellymeta.metube.volumeMounts[0].readOnly | bool | `false` |  |
| jellymeta.metube.volumeMounts[0].subPath | string | `"downloads"` |  |
| jellymeta.metube.volumes[0].name | string | `"data"` |  |
| jellymeta.metube.volumes[0].persistentVolumeClaim.claimName | string | `"jellyfin-metube"` |  |

