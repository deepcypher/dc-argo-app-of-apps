# erpnext

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 4](https://img.shields.io/badge/AppVersion-4-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://helm.erpnext.com | erpnext | 7.0.172 |
| oci://registry-1.docker.io/bitnamicharts | mariadb | 20.4.1 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| environment.baseDomain | string | `"deepcypher.me"` |  |
| environment.contact.email | string | `"noreply@deepcypher.me"` |  |
| environment.contact.name | string | `"George Onoufriou"` |  |
| environment.hardware | string | `"metal"` |  |
| environment.location.name | string | `"unknown"` |  |
| environment.mode | string | `"production"` |  |
| environment.name | string | `"unknown"` |  |
| environment.revision | string | `"main"` |  |
| erpnext.dbExistingSecret | string | `"mariadb"` |  |
| erpnext.dbExistingSecretPasswordKey | string | `"mariadb-root-password"` |  |
| erpnext.dbHost | string | `"erpnext-mariadb"` |  |
| erpnext.dbPort | int | `3306` |  |
| erpnext.dbRootUser | string | `"root"` |  |
| erpnext.enabled | bool | `true` |  |
| erpnext.ingress.annotations."cert-manager.io/cluster-issuer" | string | `"aux-issuer"` |  |
| erpnext.ingress.annotations."gethomepage.dev/description" | string | `"ERPNext."` |  |
| erpnext.ingress.annotations."gethomepage.dev/enabled" | string | `"true"` |  |
| erpnext.ingress.annotations."gethomepage.dev/group" | string | `"DeepCypher"` |  |
| erpnext.ingress.annotations."gethomepage.dev/icon" | string | `"homepage.png"` |  |
| erpnext.ingress.annotations."gethomepage.dev/name" | string | `"Erp"` |  |
| erpnext.ingress.annotations."traefik.ingress.kubernetes.io/router.middlewares" | string | `"traefik-headers@kubernetescrd"` |  |
| erpnext.ingress.className | string | `"traefik"` |  |
| erpnext.ingress.enabled | bool | `true` |  |
| erpnext.ingress.hosts[0].host | string | `"erp.deepcypher.me"` |  |
| erpnext.ingress.hosts[0].paths[0].path | string | `"/"` |  |
| erpnext.ingress.hosts[0].paths[0].pathType | string | `"Prefix"` |  |
| erpnext.ingress.tls[0].hosts[0] | string | `"erp.deepcypher.me"` |  |
| erpnext.ingress.tls[0].secretName | string | `"erp.deepcypher.me-tls"` |  |
| erpnext.jobs.backup.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].key | string | `"kubernetes.io/arch"` |  |
| erpnext.jobs.backup.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].operator | string | `"In"` |  |
| erpnext.jobs.backup.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].values[0] | string | `"amd64"` |  |
| erpnext.jobs.configure.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].key | string | `"kubernetes.io/arch"` |  |
| erpnext.jobs.configure.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].operator | string | `"In"` |  |
| erpnext.jobs.configure.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].values[0] | string | `"amd64"` |  |
| erpnext.jobs.createSite.adminExistingSecret | string | `"erpnext"` |  |
| erpnext.jobs.createSite.adminExistingSecretKey | string | `"password"` |  |
| erpnext.jobs.createSite.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].key | string | `"kubernetes.io/arch"` |  |
| erpnext.jobs.createSite.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].operator | string | `"In"` |  |
| erpnext.jobs.createSite.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].values[0] | string | `"amd64"` |  |
| erpnext.jobs.createSite.dbType | string | `"mariadb"` |  |
| erpnext.jobs.createSite.enabled | bool | `false` |  |
| erpnext.jobs.createSite.forceCreate | bool | `false` |  |
| erpnext.jobs.createSite.installApps[0] | string | `"erpnext"` |  |
| erpnext.jobs.createSite.siteName | string | `"erp.deepcypher.me"` |  |
| erpnext.jobs.custom.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].key | string | `"kubernetes.io/arch"` |  |
| erpnext.jobs.custom.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].operator | string | `"In"` |  |
| erpnext.jobs.custom.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].values[0] | string | `"amd64"` |  |
| erpnext.jobs.dropSite.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].key | string | `"kubernetes.io/arch"` |  |
| erpnext.jobs.dropSite.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].operator | string | `"In"` |  |
| erpnext.jobs.dropSite.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].values[0] | string | `"amd64"` |  |
| erpnext.jobs.dropSite.siteName | string | `"erp.deepcypher.me"` |  |
| erpnext.jobs.migrate.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].key | string | `"kubernetes.io/arch"` |  |
| erpnext.jobs.migrate.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].operator | string | `"In"` |  |
| erpnext.jobs.migrate.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].values[0] | string | `"amd64"` |  |
| erpnext.jobs.migrate.enabled | bool | `false` |  |
| erpnext.jobs.migrate.siteName | string | `"erp.deepcypher.me"` |  |
| erpnext.jobs.volumePermissions.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].key | string | `"kubernetes.io/arch"` |  |
| erpnext.jobs.volumePermissions.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].operator | string | `"In"` |  |
| erpnext.jobs.volumePermissions.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].values[0] | string | `"amd64"` |  |
| erpnext.mariadb.enabled | bool | `false` |  |
| erpnext.nginx.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].key | string | `"kubernetes.io/arch"` |  |
| erpnext.nginx.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].operator | string | `"In"` |  |
| erpnext.nginx.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].values[0] | string | `"amd64"` |  |
| erpnext.persistence.worker.accessModes[0] | string | `"ReadWriteMany"` |  |
| erpnext.persistence.worker.size | string | `"8Gi"` |  |
| erpnext.persistence.worker.storageClass | string | `"ceph-filesystem"` |  |
| erpnext.socketio.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].key | string | `"kubernetes.io/arch"` |  |
| erpnext.socketio.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].operator | string | `"In"` |  |
| erpnext.socketio.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].values[0] | string | `"amd64"` |  |
| erpnext.worker.default.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].key | string | `"kubernetes.io/arch"` |  |
| erpnext.worker.default.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].operator | string | `"In"` |  |
| erpnext.worker.default.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].values[0] | string | `"amd64"` |  |
| mariadb.auth.existingSecret | string | `"mariadb"` |  |
| mariadb.enabled | bool | `true` |  |
| mariadb.primary.livenessProbe.enabled | bool | `true` |  |
| mariadb.primary.livenessProbe.initialDelaySeconds | int | `10` |  |
| mariadb.primary.readinessProbe.enabled | bool | `true` |  |
| mariadb.primary.startupProbe.enabled | bool | `true` |  |
| mariadb.primary.startupProbe.failureThreshold | int | `50` |  |
| mariadb.primary.startupProbe.initialDelaySeconds | int | `120` |  |
| mariadb.primary.startupProbe.periodSeconds | int | `15` |  |
| mariadb.primary.startupProbe.successThreshold | int | `1` |  |
| mariadb.secondary.livenessProbe.enabled | bool | `true` |  |
| mariadb.secondary.livenessProbe.initialDelaySeconds | int | `10` |  |
| mariadb.secondary.readinessProbe.enabled | bool | `true` |  |
| mariadb.secondary.startupProbe.enabled | bool | `true` |  |
| mariadb.secondary.startupProbe.failureThreshold | int | `50` |  |
| mariadb.secondary.startupProbe.initialDelaySeconds | int | `120` |  |
| mariadb.secondary.startupProbe.periodSeconds | int | `15` |  |
| mariadb.secondary.startupProbe.successThreshold | int | `1` |  |
| psql.enabled | bool | `false` |  |

