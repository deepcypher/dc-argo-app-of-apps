# homepage

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| http://jameswynn.github.io/helm-charts | homepage | 2.0.1 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| homepage.config.bookmarks[0].Developer[0].Github[0].abbr | string | `"GH"` |  |
| homepage.config.bookmarks[0].Developer[0].Github[0].href | string | `"https://github.com/"` |  |
| homepage.config.kubernetes.mode | string | `"cluster"` |  |
| homepage.config.services | list | `[]` |  |
| homepage.config.settings | string | `nil` |  |
| homepage.config.widgets[0].kubernetes.cluster.cpu | bool | `true` |  |
| homepage.config.widgets[0].kubernetes.cluster.label | string | `"cluster"` |  |
| homepage.config.widgets[0].kubernetes.cluster.memory | bool | `true` |  |
| homepage.config.widgets[0].kubernetes.cluster.show | bool | `true` |  |
| homepage.config.widgets[0].kubernetes.cluster.showLabel | bool | `true` |  |
| homepage.config.widgets[0].kubernetes.nodes.cpu | bool | `true` |  |
| homepage.config.widgets[0].kubernetes.nodes.memory | bool | `true` |  |
| homepage.config.widgets[0].kubernetes.nodes.show | bool | `true` |  |
| homepage.config.widgets[0].kubernetes.nodes.showLabel | bool | `true` |  |
| homepage.config.widgets[1].search.provider | string | `"duckduckgo"` |  |
| homepage.config.widgets[1].search.target | string | `"_blank"` |  |
| homepage.enableRbac | bool | `true` |  |
| homepage.ingress.main.annotations."gethomepage.dev/description" | string | `"Dynamically Detected Homepage"` |  |
| homepage.ingress.main.annotations."gethomepage.dev/enabled" | string | `"true"` |  |
| homepage.ingress.main.annotations."gethomepage.dev/group" | string | `"Dynamic"` |  |
| homepage.ingress.main.annotations."gethomepage.dev/icon" | string | `"homepage.png"` |  |
| homepage.ingress.main.annotations."gethomepage.dev/name" | string | `"Homepage"` |  |
| homepage.ingress.main.enabled | bool | `false` |  |
| homepage.ingress.main.hosts[0].host | string | `"homepage.deepcypher.me"` |  |
| homepage.ingress.main.hosts[0].paths[0].path | string | `"/"` |  |
| homepage.ingress.main.hosts[0].paths[0].pathType | string | `"Prefix"` |  |
| homepage.serviceAccount.create | bool | `true` |  |
| homepage.serviceAccount.name | string | `"homepage"` |  |

