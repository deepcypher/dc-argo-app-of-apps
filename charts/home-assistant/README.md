# home-assistant

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://gitlab.com/api/v4/projects/55284972/packages/helm/stable | ha-bkp(backupd) | 0.7.0 |
| https://gitlab.com/api/v4/projects/55284972/packages/helm/stable | ha(home-assistant) | 0.7.0 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| ha-bkp.backup.copyMethod | string | `"Direct"` |  |
| ha-bkp.backup.enabled | bool | `false` |  |
| ha-bkp.backup.schedule | string | `"0 2 * * *"` |  |
| ha-bkp.enabled | bool | `false` |  |
| ha-bkp.existingPVC | string | `"home-assistant-ha"` |  |
| ha-bkp.existingSecret | string | `"home-assistant-backupd-wasabi"` |  |
| ha-bkp.persistence.generate | bool | `true` |  |
| ha-bkp.persistence.size | string | `"100G"` |  |
| ha-bkp.restore.asOf | string | `"2024-12-28T22:24:00-00:00"` |  |
| ha-bkp.restore.enabled | bool | `false` |  |
| ha-bkp.restore.schedule | string | `"0 4 * * *"` |  |
| ha-bkp.restore.trigger.manual | string | `"manual-2024-12-28T22-24-00"` |  |
| ha.configMap.configuration | string | `"# Loads default set of integrations. Do not remove.\ndefault_config:\n\n# Load frontend themes from the themes folder\nfrontend:\n  themes: !include_dir_merge_named themes\n\n# automation: !include automations.yaml\n# script: !include scripts.yaml\n# scene: !include scenes.yaml\n\nhttp:\n  # setting any local proxy as valid\n  # since this will be in k8s cluster\n  use_x_forwarded_for: true\n  trusted_proxies:\n  - 10.0.0.0/8\n  - 192.168.0.0/16\n  - 172.16.0.0/20\n\n# enable historic sensor tracking\nhistory:\n\nsensor:\n- name: front-door-persistent-activity\n  platform: history_stats\n  entity_id: binary_sensor.front_door_motion\n  state: \"Detected\"\n  type: time\n  end: \"{{ now() }}\"\n  duration:\n    minutes: 10\n"` |  |
| ha.configMap.existingConfigMap | string | `""` |  |
| ha.configMap.key | string | `"configuration.yaml"` |  |
| ha.enabled | bool | `true` |  |
| ha.ingress.annotations."cert-manager.io/cluster-issuer" | string | `"aux-issuer"` |  |
| ha.ingress.annotations."traefik.ingress.kubernetes.io/router.middlewares" | string | `"traefik-headers@kubernetescrd"` |  |
| ha.ingress.className | string | `"traefik"` |  |
| ha.ingress.enabled | bool | `true` |  |
| ha.ingress.hosts[0].host | string | `"ha.deepcypher.me"` |  |
| ha.ingress.hosts[0].paths[0].path | string | `"/"` |  |
| ha.ingress.hosts[0].paths[0].pathType | string | `"Prefix"` |  |
| ha.ingress.tls[0].hosts[0] | string | `"ha.deepcypher.me"` |  |
| ha.ingress.tls[0].secretName | string | `"ha.deepcypher.me-tls"` |  |
| ha.initContainers[0].command[0] | string | `"/bin/bash"` |  |
| ha.initContainers[0].command[1] | string | `"-c"` |  |
| ha.initContainers[0].command[2] | string | `"bash /hacs-install.sh\n# wget -O - https://get.hacs.xyz | bash -\n"` |  |
| ha.initContainers[0].image | string | `"ghcr.io/home-assistant/home-assistant:2024.12.5"` |  |
| ha.initContainers[0].imagePullPolicy | string | `"IfNotPresent"` |  |
| ha.initContainers[0].name | string | `"hacs"` |  |
| ha.initContainers[0].volumeMounts[0].mountPath | string | `"/config"` |  |
| ha.initContainers[0].volumeMounts[0].name | string | `"data"` |  |
| ha.initContainers[0].volumeMounts[0].subPath | string | `"data"` |  |
| ha.initContainers[0].volumeMounts[1].mountPath | string | `"/config/configuration.yaml"` |  |
| ha.initContainers[0].volumeMounts[1].name | string | `"config"` |  |
| ha.initContainers[0].volumeMounts[1].subPath | string | `"configuration.yaml"` |  |
| ha.initContainers[0].volumeMounts[2].mountPath | string | `"/hacs-install.sh"` |  |
| ha.initContainers[0].volumeMounts[2].name | string | `"hacs-install"` |  |
| ha.initContainers[0].volumeMounts[2].subPath | string | `"hacs-install.sh"` |  |
| ha.netpol.enabled | bool | `false` |  |
| ha.persistence.accessModes[0] | string | `"ReadWriteOnce"` |  |
| ha.persistence.annotations | object | `{}` |  |
| ha.persistence.enabled | bool | `true` |  |
| ha.persistence.existingClaim | string | `""` |  |
| ha.persistence.generatePVC | bool | `true` |  |
| ha.persistence.labels | object | `{}` |  |
| ha.persistence.size | string | `"100Gi"` |  |
| ha.persistence.storageClass | string | `""` |  |
| ha.podSecurityContextEnabled | bool | `false` |  |
| ha.resources.limits.memory | string | `"800Mi"` |  |
| ha.resources.requests.cpu | string | `"100m"` |  |
| ha.securityContextEnabled | bool | `false` |  |
| ha.volumes[0].name | string | `"config"` |  |
| ha.volumes[0].secret.defaultMode | int | `420` |  |
| ha.volumes[0].secret.items[0].key | string | `"configuration.yaml"` |  |
| ha.volumes[0].secret.items[0].path | string | `"configuration.yaml"` |  |
| ha.volumes[0].secret.secretName | string | `"ha"` |  |
| ha.volumes[1].configMap.defaultMode | int | `420` |  |
| ha.volumes[1].configMap.items[0].key | string | `"hacs-install.sh"` |  |
| ha.volumes[1].configMap.items[0].path | string | `"hacs-install.sh"` |  |
| ha.volumes[1].configMap.name | string | `"hacs-install"` |  |
| ha.volumes[1].name | string | `"hacs-install"` |  |

