# prometheus

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| oci://registry-1.docker.io/bitnamicharts | kube-prometheus | 10.2.5 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| kube-prometheus.exporters.node-exporter.enabled | bool | `false` |  |
| kube-prometheus.kube-state-metrics.resources.limits.ephemeral-storage | string | `"5Gi"` |  |
| kube-prometheus.kube-state-metrics.resources.limits.memory | string | `"500Mi"` |  |
| kube-prometheus.kube-state-metrics.resources.requests.cpu | string | `"100m"` |  |
| kube-prometheus.node-exporter.tolerations[0].effect | string | `"NoSchedule"` |  |
| kube-prometheus.node-exporter.tolerations[0].key | string | `"nvidia.com/gpu"` |  |
| kube-prometheus.node-exporter.tolerations[0].operator | string | `"Equal"` |  |
| kube-prometheus.node-exporter.tolerations[0].value | string | `"present"` |  |
| kube-prometheus.prometheus.enableRemoteWriteReceiver | bool | `true` |  |
| kube-prometheus.prometheus.persistence.enabled | bool | `true` |  |
| kube-prometheus.prometheus.persistence.size | string | `"20Gi"` |  |
| kube-prometheus.prometheus.resources.limits.ephemeral-storage | string | `"5Gi"` |  |
| kube-prometheus.prometheus.resources.limits.memory | string | `"5Gi"` |  |
| kube-prometheus.prometheus.resources.requests.cpu | string | `"750m"` |  |

