# ollama

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 4](https://img.shields.io/badge/AppVersion-4-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://gitlab.com/api/v4/projects/55284972/packages/helm/stable | ollama | 0.2.1 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| ollama.persistence.enabled | bool | `true` |  |
| ollama.persistence.size | string | `"100Gi"` |  |
| ollama.resources.limits."nvidia.com/gpu" | string | `"1"` |  |
| ollama.resources.limits.memory | string | `"8000Mi"` |  |
| ollama.resources.requests.cpu | string | `"4000m"` |  |
| ollama.runtimeClassName | string | `"nvidia"` |  |

