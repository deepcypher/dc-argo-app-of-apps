# argo-cd

![Version: 0.1.1](https://img.shields.io/badge/Version-0.1.1-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: v2.3.3](https://img.shields.io/badge/AppVersion-v2.3.3-informational?style=flat-square)

A Helm chart for ArgoCD towards Kubernetes automation

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| certPKI.branch | bool | `false` |  |
| environment.baseDomain | string | `"deepcypher.me"` |  |
| environment.contact.email | string | `"noreply@deepcypher.me"` |  |
| environment.contact.name | string | `"George Onoufriou"` |  |
| environment.flavour | string | `"talos"` |  |
| environment.hardware | string | `"metal"` |  |
| environment.location.name | string | `"unknown"` |  |
| environment.mode | string | `"production"` |  |
| environment.name | string | `"talos"` |  |
| environment.revision | string | `"HEAD"` |  |
| global.repo | string | `"https://gitlab.com/deepcypher/dc-kc.git"` |  |
| metallb.branch | bool | `false` |  |
| metallb.enable | bool | `true` |  |
| metallb.l2.config.name | string | `"metallb-l2-config"` |  |
| metallb.l2.enable | bool | `true` |  |
| metallb.l2.pool.addresses | list | `[]` |  |
| metallb.l2.pool.name | string | `"metallb-default-pool"` |  |
| metallb.namespace | string | `"metallb-system"` |  |

