{{- define "defaultEnvironment" }}
{{- with .Values.environment }}
environment:
{{- toYaml . | nindent 2 }}
{{- end }}
{{- end }}
