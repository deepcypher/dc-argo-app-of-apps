# cilium

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 4](https://img.shields.io/badge/AppVersion-4-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://helm.cilium.io/ | cilium | 1.16.2 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| cilium.cgroup.autoMount.enabled | bool | `false` |  |
| cilium.cgroup.hostRoot | string | `"/sys/fs/cgroup"` |  |
| cilium.encryption.enabled | bool | `true` |  |
| cilium.encryption.nodeEncryption | bool | `true` |  |
| cilium.encryption.type | string | `"wireguard"` |  |
| cilium.externalIPs.enabled | bool | `true` |  |
| cilium.hubble.relay.enabled | bool | `true` |  |
| cilium.hubble.ui.enabled | bool | `true` |  |
| cilium.ipam.mode | string | `"kubernetes"` |  |
| cilium.k8sClientRateLimit.burst | int | `200` |  |
| cilium.k8sClientRateLimit.qps | int | `100` |  |
| cilium.k8sServiceHost | string | `"localhost"` |  |
| cilium.k8sServicePort | int | `7445` |  |
| cilium.kubeProxyReplacement | bool | `true` |  |
| cilium.l2announcements.enabled | bool | `true` |  |
| cilium.prometheus.enabled | bool | `true` |  |
| cilium.rollOutCiliumPods | bool | `true` |  |
| cilium.securityContext.capabilities.ciliumAgent[0] | string | `"CHOWN"` |  |
| cilium.securityContext.capabilities.ciliumAgent[10] | string | `"SETUID"` |  |
| cilium.securityContext.capabilities.ciliumAgent[1] | string | `"KILL"` |  |
| cilium.securityContext.capabilities.ciliumAgent[2] | string | `"NET_ADMIN"` |  |
| cilium.securityContext.capabilities.ciliumAgent[3] | string | `"NET_RAW"` |  |
| cilium.securityContext.capabilities.ciliumAgent[4] | string | `"IPC_LOCK"` |  |
| cilium.securityContext.capabilities.ciliumAgent[5] | string | `"SYS_ADMIN"` |  |
| cilium.securityContext.capabilities.ciliumAgent[6] | string | `"SYS_RESOURCE"` |  |
| cilium.securityContext.capabilities.ciliumAgent[7] | string | `"DAC_OVERRIDE"` |  |
| cilium.securityContext.capabilities.ciliumAgent[8] | string | `"FOWNER"` |  |
| cilium.securityContext.capabilities.ciliumAgent[9] | string | `"SETGID"` |  |
| cilium.securityContext.capabilities.cleanCiliumState[0] | string | `"NET_ADMIN"` |  |
| cilium.securityContext.capabilities.cleanCiliumState[1] | string | `"SYS_ADMIN"` |  |
| cilium.securityContext.capabilities.cleanCiliumState[2] | string | `"SYS_RESOURCE"` |  |

