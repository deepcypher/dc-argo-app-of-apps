# qbittorrent

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://gitlab.com/api/v4/projects/55284972/packages/helm/stable | qbittorrent | 0.5.0 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| qbittorrent.env[0].name | string | `"PUID"` |  |
| qbittorrent.env[0].value | string | `"1000"` |  |
| qbittorrent.env[1].name | string | `"PGID"` |  |
| qbittorrent.env[1].value | string | `"1000"` |  |
| qbittorrent.env[2].name | string | `"TZ"` |  |
| qbittorrent.env[2].value | string | `"Europe/London"` |  |
| qbittorrent.env[3].name | string | `"DOCKER_MODS"` |  |
| qbittorrent.env[3].value | string | `"lscr.io/linuxserver/mods:universal-stdout-logs"` |  |
| qbittorrent.env[4].name | string | `"LOGS_TO_STDOUT"` |  |
| qbittorrent.env[4].value | string | `"/config/qBittorrent/logs/qbittorrent.log"` |  |
| qbittorrent.ingress.annotations."cert-manager.io/cluster-issuer" | string | `"aux-issuer"` |  |
| qbittorrent.ingress.enabled | bool | `true` |  |
| qbittorrent.ingress.hosts[0].host | string | `"torrent.deepcypher.me"` |  |
| qbittorrent.ingress.hosts[0].paths[0].path | string | `"/"` |  |
| qbittorrent.ingress.hosts[0].paths[0].pathType | string | `"ImplementationSpecific"` |  |
| qbittorrent.ingress.tls[0].hosts[0] | string | `"torrent.deepcypher.me"` |  |
| qbittorrent.ingress.tls[0].secretName | string | `"torrent.deepcypher.me-tls"` |  |
| qbittorrent.initContainers[0].envFrom[0].secretRef.name | string | `"gluetun"` |  |
| qbittorrent.initContainers[0].envFrom[0].secretRef.optional | bool | `false` |  |
| qbittorrent.initContainers[0].env[0].name | string | `"TZ"` |  |
| qbittorrent.initContainers[0].env[0].value | string | `"Europe/London"` |  |
| qbittorrent.initContainers[0].env[1].name | string | `"FIREWALL_DEBUG"` |  |
| qbittorrent.initContainers[0].env[1].value | string | `"on"` |  |
| qbittorrent.initContainers[0].env[2].name | string | `"FIREWALL_INPUT_PORTS"` |  |
| qbittorrent.initContainers[0].env[2].value | string | `"8080"` |  |
| qbittorrent.initContainers[0].image | string | `"ghcr.io/qdm12/gluetun:v3.39"` |  |
| qbittorrent.initContainers[0].imagePullPolicy | string | `"Always"` |  |
| qbittorrent.initContainers[0].name | string | `"gluetun"` |  |
| qbittorrent.initContainers[0].ports[0].containerPort | int | `8888` |  |
| qbittorrent.initContainers[0].ports[0].name | string | `"http-proxy"` |  |
| qbittorrent.initContainers[0].ports[0].protocol | string | `"TCP"` |  |
| qbittorrent.initContainers[0].ports[1].containerPort | int | `8388` |  |
| qbittorrent.initContainers[0].ports[1].name | string | `"tcp-shadowsocks"` |  |
| qbittorrent.initContainers[0].ports[1].protocol | string | `"TCP"` |  |
| qbittorrent.initContainers[0].ports[2].containerPort | int | `8388` |  |
| qbittorrent.initContainers[0].ports[2].name | string | `"udp-shadowsocks"` |  |
| qbittorrent.initContainers[0].ports[2].protocol | string | `"UDP"` |  |
| qbittorrent.initContainers[0].restartPolicy | string | `"Always"` |  |
| qbittorrent.initContainers[0].securityContext.capabilities.add[0] | string | `"NET_ADMIN"` |  |
| qbittorrent.persistence.accessModes[0] | string | `"ReadWriteMany"` |  |
| qbittorrent.persistence.enabled | bool | `true` |  |
| qbittorrent.persistence.size | string | `"400Gi"` |  |
| qbittorrent.resources.limits.memory | string | `"2Gi"` |  |
| qbittorrent.resources.requests.cpu | string | `"200m"` |  |
| qbittorrent.securityContext | string | `nil` |  |

