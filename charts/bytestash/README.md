# bytestash

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://gitlab.com/api/v4/projects/55284972/packages/helm/stable | bytestash-bkp(backupd) | 0.7.0 |
| https://gitlab.com/api/v4/projects/55284972/packages/helm/stable | bytestash | 0.1.0 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| bytestash-bkp.backup.copyMethod | string | `"Snapshot"` |  |
| bytestash-bkp.backup.enabled | bool | `true` |  |
| bytestash-bkp.backup.pruneIntervalDays | int | `1` |  |
| bytestash-bkp.backup.retain.daily | int | `4` |  |
| bytestash-bkp.backup.retain.hourly | int | `5` |  |
| bytestash-bkp.backup.retain.monthly | int | `2` |  |
| bytestash-bkp.backup.retain.weekly | int | `3` |  |
| bytestash-bkp.backup.retain.yearly | int | `1` |  |
| bytestash-bkp.backup.schedule | string | `"0 * * * 1"` |  |
| bytestash-bkp.backup.trigger | object | `{}` |  |
| bytestash-bkp.existingPVC | string | `"bytestash"` |  |
| bytestash-bkp.existingSecret | string | `"bytestash-backupd-wasabi"` |  |
| bytestash-bkp.persistence.accessModes[0] | string | `"ReadWriteOnce"` |  |
| bytestash-bkp.persistence.generate | bool | `false` |  |
| bytestash-bkp.persistence.size | string | `"8Gi"` |  |
| bytestash-bkp.restore.asOf | string | `""` |  |
| bytestash-bkp.restore.enabled | bool | `false` |  |
| bytestash-bkp.restore.previous | string | `nil` |  |
| bytestash-bkp.restore.schedule | string | `"1 * * * 1"` |  |
| bytestash-bkp.restore.trigger | string | `nil` |  |
| bytestash.envFrom[0].secretRef.name | string | `"bytestash"` |  |
| bytestash.envFrom[0].secretRef.optional | bool | `false` |  |
| bytestash.env[0].name | string | `"TOKEN_EXPIRY"` |  |
| bytestash.env[0].value | string | `"24h"` |  |
| bytestash.env[1].name | string | `"ALLOW_NEW_ACCOUNTS"` |  |
| bytestash.env[1].value | string | `"false"` |  |
| bytestash.env[2].name | string | `"DEBUG"` |  |
| bytestash.env[2].value | string | `"false"` |  |
| bytestash.env[3].name | string | `"DISABLE_ACCOUNTS"` |  |
| bytestash.env[3].value | string | `"false"` |  |
| bytestash.env[4].name | string | `"DISABLE_INTERNAL_ACCOUNTS"` |  |
| bytestash.env[4].value | string | `"false"` |  |
| bytestash.env[5].name | string | `"OIDC_ENABLED"` |  |
| bytestash.env[5].value | string | `"true"` |  |
| bytestash.env[6].name | string | `"OIDC_DISPLAY_NAME"` |  |
| bytestash.env[6].value | string | `"Keycloak"` |  |
| bytestash.env[7].name | string | `"OIDC_ISSUER_URL"` |  |
| bytestash.env[7].valueFrom.configMapKeyRef.key | string | `"issuerURL"` |  |
| bytestash.env[7].valueFrom.configMapKeyRef.name | string | `"oidc-urls"` |  |
| bytestash.env[8].name | string | `"OIDC_CLIENT_ID"` |  |
| bytestash.env[8].valueFrom.secretKeyRef.key | string | `"clientId"` |  |
| bytestash.env[8].valueFrom.secretKeyRef.name | string | `"oidc-credentials"` |  |
| bytestash.env[9].name | string | `"OIDC_CLIENT_SECRET"` |  |
| bytestash.env[9].valueFrom.secretKeyRef.key | string | `"clientSecret"` |  |
| bytestash.env[9].valueFrom.secretKeyRef.name | string | `"oidc-credentials"` |  |
| bytestash.ingress.annotations."cert-manager.io/cluster-issuer" | string | `"aux-issuer"` |  |
| bytestash.ingress.enabled | bool | `true` |  |
| bytestash.ingress.hosts[0].host | string | `"bytestash.deepcypher.me"` |  |
| bytestash.ingress.hosts[0].paths[0].path | string | `"/"` |  |
| bytestash.ingress.hosts[0].paths[0].pathType | string | `"ImplementationSpecific"` |  |
| bytestash.ingress.tls[0].hosts[0] | string | `"bytestash.deepcypher.me"` |  |
| bytestash.ingress.tls[0].secretName | string | `"bytestash.deepcypher.me-tls"` |  |
| bytestash.persistence.enabled | bool | `true` |  |
| bytestash.persistence.size | string | `"20Gi"` |  |
| environment.baseDomain | string | `"deepcypher.me"` |  |
| environment.contact.email | string | `"noreply@deepcypher.me"` |  |
| environment.contact.name | string | `"George Onoufriou"` |  |
| environment.hardware | string | `"metal"` |  |
| environment.location.name | string | `"unknown"` |  |
| environment.mode | string | `"production"` |  |
| environment.name | string | `"unknown"` |  |
| environment.revision | string | `"main"` |  |
| oidc.realm | string | `"deepcypher"` |  |

