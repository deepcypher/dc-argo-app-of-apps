# alloy

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 4](https://img.shields.io/badge/AppVersion-4-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://grafana.github.io/helm-charts | alloy | 0.11.0 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| alloy.alloy.configMap.create | bool | `false` |  |
| alloy.alloy.configMap.key | string | `"config.alloy"` |  |
| alloy.alloy.configMap.name | string | `"alloy-config"` |  |
| alloy.alloy.envFrom[0].secretRef.name | string | `"cloud"` |  |
| alloy.alloy.mounts.dockercontainers | bool | `true` |  |
| alloy.alloy.mounts.extra[0].mountPath | string | `"/proc"` |  |
| alloy.alloy.mounts.extra[0].name | string | `"proc"` |  |
| alloy.alloy.mounts.extra[0].readOnly | bool | `true` |  |
| alloy.alloy.mounts.extra[1].mountPath | string | `"/sys"` |  |
| alloy.alloy.mounts.extra[1].name | string | `"sys"` |  |
| alloy.alloy.mounts.extra[1].readOnly | bool | `true` |  |
| alloy.alloy.mounts.varlog | bool | `true` |  |
| alloy.alloy.resources.limits.memory | string | `"750Mi"` |  |
| alloy.alloy.resources.requests.cpu | string | `"500m"` |  |
| alloy.alloy.securityContext.privileged | bool | `true` |  |
| alloy.alloy.securityContext.runAsUser | int | `0` |  |
| alloy.controller.priorityClassName | string | `"node-monitoring-critical"` |  |
| alloy.controller.tolerations[0].effect | string | `"NoSchedule"` |  |
| alloy.controller.tolerations[0].key | string | `"node-role.kubernetes.io/control-plane"` |  |
| alloy.controller.tolerations[0].operator | string | `"Exists"` |  |
| alloy.controller.tolerations[1].effect | string | `"NoSchedule"` |  |
| alloy.controller.tolerations[1].key | string | `"nvidia.com/gpu"` |  |
| alloy.controller.tolerations[1].operator | string | `"Equal"` |  |
| alloy.controller.tolerations[1].value | string | `"present"` |  |
| alloy.controller.type | string | `"daemonset"` |  |
| alloy.controller.volumes.extra[0].hostPath.path | string | `"/proc"` |  |
| alloy.controller.volumes.extra[0].hostPath.type | string | `""` |  |
| alloy.controller.volumes.extra[0].name | string | `"proc"` |  |
| alloy.controller.volumes.extra[1].hostPath.path | string | `"/sys"` |  |
| alloy.controller.volumes.extra[1].hostPath.type | string | `""` |  |
| alloy.controller.volumes.extra[1].name | string | `"sys"` |  |
| environment.baseDomain | string | `"deepcypher.me"` |  |
| environment.contact.email | string | `"noreply@deepcypher.me"` |  |
| environment.contact.name | string | `"George Onoufriou"` |  |
| environment.hardware | string | `"metal"` |  |
| environment.location.name | string | `"unknown"` |  |
| environment.mode | string | `"production"` |  |
| environment.name | string | `"unknown"` |  |
| environment.revision | string | `"main"` |  |

