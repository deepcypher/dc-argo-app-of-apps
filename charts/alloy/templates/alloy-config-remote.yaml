apiVersion: v1
kind: ConfigMap
metadata:
  name: alloy-config
data:
  # https://grafana.com/docs/alloy/latest/collect/logs-in-kubernetes/
  # TODO: hook all new env vars
  config.alloy: |
    logging {
      //level  = "info"
      //level  = "warn"
      level  = "error"
      format = "logfmt"
    }

    // OPEN TELEMETRY

    otelcol.receiver.otlp "default" {
      grpc {
        endpoint = "0.0.0.0:4317"
      }
      http {
        endpoint = "0.0.0.0:4318"
      }
      output {
        metrics  = [otelcol.processor.batch.default.input]
        logs     = [otelcol.processor.batch.default.input]
        traces   = [otelcol.processor.batch.default.input]
      }
    }

    otelcol.processor.batch "default" {
      output {
        metrics = [otelcol.exporter.prometheus.default.input]
        logs    = [otelcol.exporter.loki.default.input]
        // traces  = [otelcol.exporter.otlp.default.input]
        traces  = [otelcol.exporter.otlphttp.default.input]
      }
    }

    otelcol.exporter.loki "default" {
      forward_to = [loki.process.default.receiver]
    }

    otelcol.exporter.prometheus "default" {
      forward_to = [prometheus.remote_write.default.receiver]
    }

    otelcol.exporter.otlp "default" {
      client {
        endpoint = "grpc://tempo-distributor.tempo:4317"
      }
    }

    otelcol.exporter.otlphttp "default" {
      client {
        endpoint = sys.env("tempoHost") // "http://tempo-distributor.tempo:4318"
        auth = otelcol.auth.basic.cloud.handler
      }
    }

    otelcol.auth.basic "cloud" {
      username = sys.env("tempoUser")
      password = sys.env("accessPolicyToken")
    }

    // PROMETHEUS

    prometheus.remote_write "default" {
      endpoint {
        url = sys.env("prometheusHost") // "http://prometheus-operated.prometheus:9090/api/v1/write"
        basic_auth {
          username = sys.env("prometheusUser")
          password = sys.env("accessPolicyToken")
        }
      }

    }

    prometheus.exporter.unix "default" {
    }

    // scrape the unix node_exporter metrics
    prometheus.scrape "default" {
      targets    = prometheus.exporter.unix.default.targets
      forward_to = [prometheus.remote_write.default.receiver]
    }

    // LOKI

    loki.process "default" {
      forward_to = [loki.write.default.receiver]
      stage.drop {
        older_than = "1h"
      }
    }

    loki.write "default" {
      endpoint {
        url = sys.env("lokiHost") // "http://loki-distributor.loki:3100/loki/api/v1/push"
        basic_auth {
          username = sys.env("lokiUser")
          password = sys.env("accessPolicyToken")
        }
      }
    }

    discovery.kubernetes "pod" {
      role = "pod"
    }

    discovery.kubernetes "node" {
      role = "node"
    }

    discovery.kubernetes "service" {
      role = "service"
    }

    discovery.kubernetes "endpoint" {
      role = "endpoints"
    }

    discovery.kubernetes "endpointslice" {
      role = "endpointslice"
    }

    discovery.kubernetes "ingress" {
      role = "ingress"
    }

    // discovery.relabel rewrites the label set of the input targets by applying one or more relabeling rules.
    // If no rules are defined, then the input targets are exported as-is.
    discovery.relabel "pod_logs" {
      targets = discovery.kubernetes.pod.targets

      // Label creation - "namespace" field from "__meta_kubernetes_namespace"
      rule {
        source_labels = ["__meta_kubernetes_namespace"]
        action = "replace"
        target_label = "namespace"
      }

      // Label creation - "pod" field from "__meta_kubernetes_pod_name"
      rule {
        source_labels = ["__meta_kubernetes_pod_name"]
        action = "replace"
        target_label = "pod"
      }

      // Label creation - "container" field from "__meta_kubernetes_pod_container_name"
      rule {
        source_labels = ["__meta_kubernetes_pod_container_name"]
        action = "replace"
        target_label = "container"
      }

      // Label creation -  "app" field from "__meta_kubernetes_pod_label_app_kubernetes_io_name"
      rule {
        source_labels = ["__meta_kubernetes_pod_label_app_kubernetes_io_name"]
        action = "replace"
        target_label = "app"
      }

      // Label creation -  "job" field from "__meta_kubernetes_namespace" and "__meta_kubernetes_pod_container_name"
      // Concatenate values __meta_kubernetes_namespace/__meta_kubernetes_pod_container_name
      rule {
        source_labels = ["__meta_kubernetes_namespace", "__meta_kubernetes_pod_container_name"]
        action = "replace"
        target_label = "job"
        separator = "/"
        replacement = "$1"
      }

      // Label creation - "container" field from "__meta_kubernetes_pod_uid" and "__meta_kubernetes_pod_container_name"
      // Concatenate values __meta_kubernetes_pod_uid/__meta_kubernetes_pod_container_name.log
      rule {
        source_labels = ["__meta_kubernetes_pod_uid", "__meta_kubernetes_pod_container_name"]
        action = "replace"
        target_label = "__path__"
        separator = "/"
        replacement = "/var/log/pods/*$1/*.log"
      }

      // Label creation -  "container_runtime" field from "__meta_kubernetes_pod_container_id"
      rule {
        source_labels = ["__meta_kubernetes_pod_container_id"]
        action = "replace"
        target_label = "container_runtime"
        regex = "^(\\S+):\\/\\/.+$"
        replacement = "$1"
      }
    }

    // loki.source.kubernetes tails logs from Kubernetes containers using the Kubernetes API.
    loki.source.kubernetes "pod_logs" {
      targets    = discovery.relabel.pod_logs.output
      forward_to = [loki.process.pod_logs.receiver]
    }

    // loki.process receives log entries from other Loki components, applies one or more processing stages,
    // and forwards the results to the list of receivers in the component’s arguments.
    loki.process "pod_logs" {
      stage.static_labels {
          values = {
            cluster = "production",
          }
      }

      forward_to = [loki.process.default.receiver]
    }

    // loki.source.kubernetes_events tails events from the Kubernetes API and converts them
    // into log lines to forward to other Loki components.
    loki.source.kubernetes_events "cluster_events" {
      job_name   = "integrations/kubernetes/eventhandler"
      log_format = "logfmt"
      forward_to = [
        loki.process.cluster_events.receiver,
      ]
    }

    // loki.process receives log entries from other loki components, applies one or more processing stages,
    // and forwards the results to the list of receivers in the component’s arguments.
    loki.process "cluster_events" {
      forward_to = [loki.process.default.receiver]

      stage.static_labels {
        values = {
          cluster = "production",
        }
      }

      stage.labels {
        values = {
          kubernetes_cluster_events = "job",
        }
      }
    }
