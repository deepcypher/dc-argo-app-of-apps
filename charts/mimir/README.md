# mimir

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 4](https://img.shields.io/badge/AppVersion-4-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://grafana.github.io/helm-charts | mimir-distributed | 5.6.0 |
| oci://registry-1.docker.io/bitnamicharts | minio | 14.8.6 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| mimir-distributed.admin-cache.enabled | bool | `true` |  |
| mimir-distributed.admin-cache.replicas | int | `1` |  |
| mimir-distributed.admin_api.replicas | int | `1` |  |
| mimir-distributed.admin_api.resources.limits.memory | string | `"128Mi"` |  |
| mimir-distributed.admin_api.resources.requests.cpu | string | `"100m"` |  |
| mimir-distributed.alertmanager.persistentVolume.enabled | bool | `true` |  |
| mimir-distributed.alertmanager.replicas | int | `1` |  |
| mimir-distributed.alertmanager.resources.limits.memory | string | `"1.4Gi"` |  |
| mimir-distributed.alertmanager.resources.requests.cpu | string | `"200m"` |  |
| mimir-distributed.alertmanager.resources.requests.memory | string | `"1Gi"` |  |
| mimir-distributed.alertmanager.statefulSet.enabled | bool | `true` |  |
| mimir-distributed.chunks-cache.enabled | bool | `true` |  |
| mimir-distributed.chunks-cache.replicas | int | `1` |  |
| mimir-distributed.compactor.persistentVolume.size | string | `"20Gi"` |  |
| mimir-distributed.compactor.resources.limits.memory | string | `"2.1Gi"` |  |
| mimir-distributed.compactor.resources.requests.cpu | string | `"200m"` |  |
| mimir-distributed.compactor.resources.requests.memory | string | `"1.5Gi"` |  |
| mimir-distributed.distributor.replicas | int | `1` |  |
| mimir-distributed.distributor.resources.limits.memory | string | `"5.7Gi"` |  |
| mimir-distributed.distributor.resources.requests.cpu | string | `"400m"` |  |
| mimir-distributed.distributor.resources.requests.memory | string | `"4Gi"` |  |
| mimir-distributed.enabled | bool | `true` |  |
| mimir-distributed.gateway.replicas | int | `1` |  |
| mimir-distributed.gateway.resources.limits.memory | string | `"731Mi"` |  |
| mimir-distributed.gateway.resources.requests.cpu | string | `"200m"` |  |
| mimir-distributed.global.extraEnv[0].name | string | `"ACCESS_KEY"` |  |
| mimir-distributed.global.extraEnv[0].valueFrom.secretKeyRef.key | string | `"root-user"` |  |
| mimir-distributed.global.extraEnv[0].valueFrom.secretKeyRef.name | string | `"minio"` |  |
| mimir-distributed.global.extraEnv[1].name | string | `"SECRET_KEY"` |  |
| mimir-distributed.global.extraEnv[1].valueFrom.secretKeyRef.key | string | `"root-password"` |  |
| mimir-distributed.global.extraEnv[1].valueFrom.secretKeyRef.name | string | `"minio"` |  |
| mimir-distributed.index-cache.enabled | bool | `true` |  |
| mimir-distributed.index-cache.replicas | int | `1` |  |
| mimir-distributed.ingester.affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].labelSelector.matchExpressions[0].key | string | `"target"` |  |
| mimir-distributed.ingester.affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].labelSelector.matchExpressions[0].operator | string | `"In"` |  |
| mimir-distributed.ingester.affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].labelSelector.matchExpressions[0].values[0] | string | `"ingester"` |  |
| mimir-distributed.ingester.affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].topologyKey | string | `"kubernetes.io/hostname"` |  |
| mimir-distributed.ingester.affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[1].labelSelector.matchExpressions[0].key | string | `"app.kubernetes.io/component"` |  |
| mimir-distributed.ingester.affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[1].labelSelector.matchExpressions[0].operator | string | `"In"` |  |
| mimir-distributed.ingester.affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[1].labelSelector.matchExpressions[0].values[0] | string | `"ingester"` |  |
| mimir-distributed.ingester.affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[1].topologyKey | string | `"kubernetes.io/hostname"` |  |
| mimir-distributed.ingester.persistentVolume.size | string | `"50Gi"` |  |
| mimir-distributed.ingester.replicas | int | `1` |  |
| mimir-distributed.ingester.resources.limits.memory | string | `"12Gi"` |  |
| mimir-distributed.ingester.resources.requests.cpu | string | `"400m"` |  |
| mimir-distributed.ingester.resources.requests.memory | string | `"8Gi"` |  |
| mimir-distributed.ingester.topologySpreadConstraints | object | `{}` |  |
| mimir-distributed.ingester.zoneAwareReplication.topologyKey | string | `"kubernetes.io/hostname"` |  |
| mimir-distributed.metadata-cache.enabled | bool | `true` |  |
| mimir-distributed.mimir.structuredConfig.alertmanager_storage.s3.bucket_name | string | `"mimir-alertmanager"` |  |
| mimir-distributed.mimir.structuredConfig.blocks_storage.s3.bucket_name | string | `"mimir-blocks"` |  |
| mimir-distributed.mimir.structuredConfig.common.storage.backend | string | `"s3"` |  |
| mimir-distributed.mimir.structuredConfig.common.storage.s3.access_key_id | string | `"${ACCESS_KEY}"` |  |
| mimir-distributed.mimir.structuredConfig.common.storage.s3.endpoint | string | `"minio.mimir:9000"` |  |
| mimir-distributed.mimir.structuredConfig.common.storage.s3.insecure | bool | `true` |  |
| mimir-distributed.mimir.structuredConfig.common.storage.s3.region | string | `"us-east-1"` |  |
| mimir-distributed.mimir.structuredConfig.common.storage.s3.secret_access_key | string | `"${SECRET_KEY}"` |  |
| mimir-distributed.mimir.structuredConfig.ruler_storage.s3.bucket_name | string | `"mimir-ruler"` |  |
| mimir-distributed.minio.enabled | bool | `false` |  |
| mimir-distributed.nginx.replicas | int | `1` |  |
| mimir-distributed.nginx.resources.limits.memory | string | `"731Mi"` |  |
| mimir-distributed.nginx.resources.requests.cpu | string | `"200m"` |  |
| mimir-distributed.overrides_exporter.replicas | int | `1` |  |
| mimir-distributed.overrides_exporter.resources.limits.memory | string | `"128Mi"` |  |
| mimir-distributed.overrides_exporter.resources.requests.cpu | string | `"100m"` |  |
| mimir-distributed.querier.replicas | int | `1` |  |
| mimir-distributed.querier.resources.limits.memory | string | `"5.6Gi"` |  |
| mimir-distributed.querier.resources.requests.cpu | string | `"400m"` |  |
| mimir-distributed.query_frontend.replicas | int | `1` |  |
| mimir-distributed.query_frontend.resources.limits.memory | string | `"2.8Gi"` |  |
| mimir-distributed.query_frontend.resources.requests.cpu | string | `"400m"` |  |
| mimir-distributed.results-cache.enabled | bool | `true` |  |
| mimir-distributed.results-cache.replicas | int | `1` |  |
| mimir-distributed.ruler.replicas | int | `1` |  |
| mimir-distributed.ruler.resources.limits.memory | string | `"2.8Gi"` |  |
| mimir-distributed.ruler.resources.requests.cpu | string | `"200m"` |  |
| mimir-distributed.store_gateway.affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].labelSelector.matchExpressions[0].key | string | `"target"` |  |
| mimir-distributed.store_gateway.affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].labelSelector.matchExpressions[0].operator | string | `"In"` |  |
| mimir-distributed.store_gateway.affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].labelSelector.matchExpressions[0].values[0] | string | `"store-gateway"` |  |
| mimir-distributed.store_gateway.affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].topologyKey | string | `"kubernetes.io/hostname"` |  |
| mimir-distributed.store_gateway.affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[1].labelSelector.matchExpressions[0].key | string | `"app.kubernetes.io/component"` |  |
| mimir-distributed.store_gateway.affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[1].labelSelector.matchExpressions[0].operator | string | `"In"` |  |
| mimir-distributed.store_gateway.affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[1].labelSelector.matchExpressions[0].values[0] | string | `"store-gateway"` |  |
| mimir-distributed.store_gateway.affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[1].topologyKey | string | `"kubernetes.io/hostname"` |  |
| mimir-distributed.store_gateway.persistentVolume.size | string | `"10Gi"` |  |
| mimir-distributed.store_gateway.replicas | int | `1` |  |
| mimir-distributed.store_gateway.resources.limits.memory | string | `"2.1Gi"` |  |
| mimir-distributed.store_gateway.resources.requests.cpu | int | `1` |  |
| mimir-distributed.store_gateway.topologySpreadConstraints | object | `{}` |  |
| mimir-distributed.store_gateway.zoneAwareReplication.topologyKey | string | `"kubernetes.io/hostname"` |  |
| minio.auth.existingSecret | string | `"minio"` |  |
| minio.enabled | bool | `true` |  |
| minio.fullnameOverride | string | `"minio"` |  |
| minio.ingress.annotations."cert-manager.io/cluster-issuer" | string | `"aux-issuer"` |  |
| minio.ingress.annotations."ingress.kubernetes.io/proxy-body-size" | string | `"32M"` |  |
| minio.ingress.annotations."kubernetes.io/ingress.class" | string | `"traefik"` |  |
| minio.ingress.annotations."nginx.ingress.kubernetes.io/proxy-body-size" | string | `"32M"` |  |
| minio.ingress.enabled | bool | `false` |  |
| minio.ingress.hostname | string | `"lokis3.deepcypher.me"` |  |
| minio.ingress.ingressClassName | string | `"traefik"` |  |
| minio.ingress.selfSigned | bool | `true` |  |
| minio.ingress.tls | bool | `true` |  |
| minio.metrics.prometheusAuthType | string | `"public"` |  |
| minio.metrics.serviceMonitor.enabled | bool | `true` |  |
| minio.mode | string | `"distributed"` |  |
| minio.persistence.enabled | bool | `true` |  |
| minio.persistence.size | string | `"100G"` |  |
| minio.provisioning.buckets[0].name | string | `"mimir-blocks"` |  |
| minio.provisioning.buckets[0].region | string | `"us-east-1"` |  |
| minio.provisioning.buckets[0].versioning | string | `"Suspended"` |  |
| minio.provisioning.buckets[0].withLock | bool | `false` |  |
| minio.provisioning.buckets[1].name | string | `"mimir-alertmanager"` |  |
| minio.provisioning.buckets[1].region | string | `"us-east-1"` |  |
| minio.provisioning.buckets[1].versioning | string | `"Suspended"` |  |
| minio.provisioning.buckets[1].withLock | bool | `false` |  |
| minio.provisioning.buckets[2].name | string | `"mimir-ruler"` |  |
| minio.provisioning.buckets[2].region | string | `"us-east-1"` |  |
| minio.provisioning.buckets[2].versioning | string | `"Suspended"` |  |
| minio.provisioning.buckets[2].withLock | bool | `false` |  |
| minio.provisioning.enabled | bool | `true` |  |
| minio.statefulset.replicaCount | int | `4` |  |

