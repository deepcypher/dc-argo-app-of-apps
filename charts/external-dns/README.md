# external-dns

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://coredns.github.io/helm | coredns | 1.39.1 |
| https://kubernetes-sigs.github.io/external-dns | external-dns | 1.15.2 |
| oci://registry-1.docker.io/bitnamicharts | etcd | 11.1.1 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| coredns.enabled | bool | `true` |  |
| coredns.isClusterService | bool | `false` |  |
| coredns.rbac.create | bool | `true` |  |
| coredns.servers[0].plugins | list | `[{"name":"errors"},{"configBlock":"lameduck 5s","name":"health"},{"name":"ready"},{"configBlock":"pods insecure\nfallthrough in-addr.arpa ip6.arpa\nttl 30","name":"kubernetes","parameters":"cluster.local in-addr.arpa ip6.arpa"},{"name":"prometheus","parameters":"0.0.0.0:9153"},{"name":"forward","parameters":". /etc/resolv.conf"},{"configBlock":"stubzones\npath /skydns\nendpoint http://etcd:2379","name":"etcd","parameters":"deepcypher.me"},{"name":"cache","parameters":30},{"name":"loop"},{"name":"reload"},{"name":"loadbalance"}]` | expose the service on a different port servicePort: 5353 If serviceType is nodePort you can specify nodePort here nodePort: 30053 hostPort: 53 |
| coredns.servers[0].port | int | `53` |  |
| coredns.servers[0].zones[0].zone | string | `"."` |  |
| etcd.enabled | bool | `true` |  |
| etcd.fullnameOverride | string | `"etcd"` |  |
| etcd.replicaCount | int | `3` |  |
| external-dns.enabled | bool | `false` |  |
| external-dns.provider.name | string | `"coredns"` |  |

