# alloy

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 4](https://img.shields.io/badge/AppVersion-4-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://gitlab.com/api/v4/projects/55284972/packages/helm/stable | open-webui | 0.2.2 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| environment.baseDomain | string | `"deepcypher.me"` |  |
| environment.contact.email | string | `"noreply@deepcypher.me"` |  |
| environment.contact.name | string | `"George Onoufriou"` |  |
| environment.hardware | string | `"metal"` |  |
| environment.location.name | string | `"unknown"` |  |
| environment.mode | string | `"production"` |  |
| environment.name | string | `"unknown"` |  |
| environment.revision | string | `"main"` |  |
| oidc.realm | string | `"deepcypher"` |  |
| open-webui.env[0].name | string | `"OLLAMA_BASE_URL"` |  |
| open-webui.env[0].value | string | `"http://ollama.ollama:11434"` |  |
| open-webui.env[10].name | string | `"OAUTH_ROLES_CLAIM"` |  |
| open-webui.env[10].value | string | `"roles"` |  |
| open-webui.env[11].name | string | `"OAUTH_ALLOWED_ROLES"` |  |
| open-webui.env[11].value | string | `"user"` |  |
| open-webui.env[12].name | string | `"OAUTH_ADMIN_ROLES"` |  |
| open-webui.env[12].value | string | `"admin"` |  |
| open-webui.env[1].name | string | `"ENABLE_LOGIN_FORM"` |  |
| open-webui.env[1].value | string | `"false"` |  |
| open-webui.env[2].name | string | `"ENABLE_SIGNUP"` |  |
| open-webui.env[2].value | string | `"false"` |  |
| open-webui.env[3].name | string | `"ENABLE_OAUTH_SIGNUP"` |  |
| open-webui.env[3].value | string | `"true"` |  |
| open-webui.env[4].name | string | `"OAUTH_MERGE_ACCOUNTS_BY_EMAIL"` |  |
| open-webui.env[4].value | string | `"true"` |  |
| open-webui.env[5].name | string | `"OAUTH_CLIENT_ID"` |  |
| open-webui.env[5].valueFrom.secretKeyRef.key | string | `"clientId"` |  |
| open-webui.env[5].valueFrom.secretKeyRef.name | string | `"oidc-credentials"` |  |
| open-webui.env[6].name | string | `"OAUTH_CLIENT_SECRET"` |  |
| open-webui.env[6].valueFrom.secretKeyRef.key | string | `"clientSecret"` |  |
| open-webui.env[6].valueFrom.secretKeyRef.name | string | `"oidc-credentials"` |  |
| open-webui.env[7].name | string | `"OPENID_PROVIDER_URL"` |  |
| open-webui.env[7].valueFrom.configMapKeyRef.key | string | `"wellKnownURL"` |  |
| open-webui.env[7].valueFrom.configMapKeyRef.name | string | `"oidc-urls"` |  |
| open-webui.env[8].name | string | `"OAUTH_PROVIDER_NAME"` |  |
| open-webui.env[8].value | string | `"Keycloak"` |  |
| open-webui.env[9].name | string | `"OAUTH_SCOPES"` |  |
| open-webui.env[9].value | string | `"openid email profile roles"` |  |
| open-webui.ingress.annotations."cert-manager.io/cluster-issuer" | string | `"aux-issuer"` |  |
| open-webui.ingress.annotations."traefik.ingress.kubernetes.io/router.middlewares" | string | `"traefik-headers@kubernetescrd"` |  |
| open-webui.ingress.className | string | `"traefik"` |  |
| open-webui.ingress.enabled | bool | `true` |  |
| open-webui.ingress.hosts[0].host | string | `"chat.deepcypher.me"` |  |
| open-webui.ingress.hosts[0].paths[0].path | string | `"/"` |  |
| open-webui.ingress.hosts[0].paths[0].pathType | string | `"ImplementationSpecific"` |  |
| open-webui.ingress.tls[0].hosts[0] | string | `"chat.deepcypher.me"` |  |
| open-webui.ingress.tls[0].secretName | string | `"chat.deepcypher.me-tls"` |  |
| open-webui.netpol.enabled | bool | `false` |  |
| open-webui.persistence.enabled | bool | `true` |  |
| open-webui.persistence.size | string | `"50Gi"` |  |

