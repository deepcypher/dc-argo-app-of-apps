# jupyterhub

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 4](https://img.shields.io/badge/AppVersion-4-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://hub.jupyter.org/helm-chart/ | jupyterhub | 4.1.0 |
| oci://registry-1.docker.io/bitnamicharts | postgresql-ha | 14.3.10 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| ingress.namespace | string | `"traefik"` |  |
| jupyterhub.hub.config.Authenticator.auto_login | bool | `true` |  |
| jupyterhub.hub.config.GenericOAuthenticator.authorize_url | string | `"https://auth.deepcypher.me/application/o/authorize/"` |  |
| jupyterhub.hub.config.GenericOAuthenticator.login_service | string | `"authentik"` |  |
| jupyterhub.hub.config.GenericOAuthenticator.oauth_callback_url | string | `"https://jhub.deepcypher.me/hub/oauth_callback"` |  |
| jupyterhub.hub.config.GenericOAuthenticator.token_url | string | `"https://auth.deepcypher.me/application/o/token/"` |  |
| jupyterhub.hub.config.GenericOAuthenticator.userdata_url | string | `"https://auth.deepcypher.me/application/o/userinfo/"` |  |
| jupyterhub.hub.config.JupyterHub.authenticator_class | string | `"generic-oauth"` |  |
| jupyterhub.hub.existingSecret | string | `"jhub"` |  |
| jupyterhub.hub.extraConfig."myConfig.py" | string | `"c.JupyterHub.authenticator_class = \"generic-oauth\"\n\n# OAuth2 application info\n# -----------------------\nclient_id = os.environ[\"CLIENT_ID\"]\nclient_secret = os.environ[\"CLIENT_SECRET\"]\nprint(\"client_id:\", client_id)\nprint(\"client_secret:\", client_secret[0:4] + \"...\")\nc.GenericOAuthenticator.client_id = client_id\nc.GenericOAuthenticator.client_secret = client_secret\n\n## Identity provider info\n## ----------------------\n#c.GenericOAuthenticator.authorize_url =\n#c.GenericOAuthenticator.token_url = \"https://accounts.example.com/auth/realms/example/protocol/openid-connect/token\"\n#c.GenericOAuthenticator.userdata_url = \"https://accounts.example.com/auth/realms/example/protocol/openid-connect/userinfo\"\n\n## What we request about the user\n## ------------------------------\n## scope represents requested information about the user, and since we configure\n## this against an OIDC based identity provider, we should request \"openid\" at\n## least.\n##\n## In this example we include \"email\" and \"groups\" as well, and then declare that\n## we should set the username based on the \"email\" key in the response, and read\n## group membership from the \"groups\" key in the response.\n##\nc.GenericOAuthenticator.scope = [\"openid\", \"email\", \"profile\", \"groups\"]\nc.GenericOAuthenticator.username_claim = \"email\"\nc.GenericOAuthenticator.claim_groups_key = \"groups\"\n\n## Authorization\n## -------------\n#c.GenericOAuthenticator.allowed_users = {\"someuser\"\nc.GenericOAuthenticator.allowed_groups = {\"user\"}\n#c.GenericOAuthenticator.admin_users = {\"someadmin\"}\nc.GenericOAuthenticator.admin_groups = {\"admin\"}\n#c.OAuthenticator.allow_all = True\n"` |  |
| jupyterhub.hub.extraEnv[0].name | string | `"CLIENT_ID"` |  |
| jupyterhub.hub.extraEnv[0].valueFrom.secretKeyRef.key | string | `"clientID"` |  |
| jupyterhub.hub.extraEnv[0].valueFrom.secretKeyRef.name | string | `"jhubauth"` |  |
| jupyterhub.hub.extraEnv[1].name | string | `"CLIENT_SECRET"` |  |
| jupyterhub.hub.extraEnv[1].valueFrom.secretKeyRef.key | string | `"clientSecret"` |  |
| jupyterhub.hub.extraEnv[1].valueFrom.secretKeyRef.name | string | `"jhubauth"` |  |
| jupyterhub.hub.networkPolicy.egress[0].ports[0].port | int | `6443` |  |
| jupyterhub.ingress.annotations."cert-manager.io/cluster-issuer" | string | `"letsencrypt"` |  |
| jupyterhub.ingress.enabled | bool | `true` |  |
| jupyterhub.ingress.hosts[0] | string | `"jhub.deepcypher.me"` |  |
| jupyterhub.ingress.ingressClassName | string | `nil` |  |
| jupyterhub.ingress.pathSuffix | string | `nil` |  |
| jupyterhub.ingress.pathType | string | `"Prefix"` |  |
| jupyterhub.ingress.tls[0].hosts[0] | string | `"jhub.deepcypher.me"` |  |
| jupyterhub.ingress.tls[0].secretName | string | `"jhub.deepcypher.me-tls"` |  |
| jupyterhub.proxy.service.type | string | `"ClusterIP"` |  |
| jupyterhub.singleuser.profileList[0].default | bool | `true` |  |
| jupyterhub.singleuser.profileList[0].description | string | `"A notebook server with bare minimum bloat. (no GPU)"` |  |
| jupyterhub.singleuser.profileList[0].display_name | string | `"Minimal environment"` |  |
| jupyterhub.singleuser.profileList[0].kubespawner_override.image | string | `"quay.io/jupyter/minimal-notebook:latest"` |  |
| jupyterhub.singleuser.profileList[1].description | string | `"If you want the additional bells and whistles: Python, R, and Julia. (no GPU)"` |  |
| jupyterhub.singleuser.profileList[1].display_name | string | `"Datascience environment"` |  |
| jupyterhub.singleuser.profileList[1].kubespawner_override.image | string | `"quay.io/jupyter/datascience-notebook:latest"` |  |
| jupyterhub.singleuser.profileList[2].description | string | `"The Jupyter Stacks spark image! (no GPU)"` |  |
| jupyterhub.singleuser.profileList[2].display_name | string | `"Spark environment"` |  |
| jupyterhub.singleuser.profileList[2].kubespawner_override.image | string | `"quay.io/jupyter/all-spark-notebook:latest"` |  |
| jupyterhub.singleuser.profileList[3].description | string | `"A notebook server with bare minimum bloat. (with GPU)"` |  |
| jupyterhub.singleuser.profileList[3].display_name | string | `"(EXPERIMENTAL) Minimal GPU environment"` |  |
| jupyterhub.singleuser.profileList[3].kubespawner_override.extra_resource_limits."nvidia.com/gpu" | string | `"1"` |  |
| jupyterhub.singleuser.profileList[3].kubespawner_override.http_timeout | int | `120` |  |
| jupyterhub.singleuser.profileList[3].kubespawner_override.start_timeout | int | `120` |  |
| jupyterhub.singleuser.storage.capacity | string | `"100Gi"` |  |
| netpol.enabled | bool | `true` |  |
| postgresql-ha.enabled | bool | `true` |  |
| postgresql-ha.fullnameOverride | string | `"postgres"` |  |
| postgresql-ha.image.registry | string | `"docker.io"` |  |
| postgresql-ha.image.repository | string | `"bitnami/postgresql-repmgr"` |  |
| postgresql-ha.image.tag | string | `"16.2.0-debian-11-r17"` |  |
| postgresql-ha.pgpool.adminUsername | string | `"admin"` |  |
| postgresql-ha.pgpool.containerPorts.postgresql | int | `5432` |  |
| postgresql-ha.pgpool.existingSecret | string | `"pgpool"` |  |
| postgresql-ha.pgpool.replicaCount | int | `1` |  |
| postgresql-ha.postgresql.containerPorts.postgresql | int | `5432` |  |
| postgresql-ha.postgresql.database | string | `"postgres"` |  |
| postgresql-ha.postgresql.existingSecret | string | `"postgresql"` |  |
| postgresql-ha.postgresql.replicaCount | int | `1` |  |
| postgresql-ha.postgresql.upgradeRepmgrExtension | bool | `true` |  |
| postgresql-ha.postgresql.username | string | `"postgres"` |  |
| secret.generate | bool | `false` |  |
| secret.randLength | int | `32` |  |

