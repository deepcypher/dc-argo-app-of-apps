# auth

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| oci://registry-1.docker.io/bitnamicharts | keycloak | 24.2.3 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| keycloak.auth.adminUser | string | `"archer"` |  |
| keycloak.auth.existingSecret | string | `"keycloak"` |  |
| keycloak.enabled | bool | `true` |  |
| keycloak.extraEnvVars[0].name | string | `"KEYCLOAK_PRODUCTION"` |  |
| keycloak.extraEnvVars[0].value | string | `"true"` |  |
| keycloak.ingress.annotations."cert-manager.io/cluster-issuer" | string | `"letsencrypt"` |  |
| keycloak.ingress.annotations."traefik.ingress.kubernetes.io/router.middlewares" | string | `"traefik-headers@kubernetescrd"` |  |
| keycloak.ingress.enabled | bool | `true` |  |
| keycloak.ingress.hostname | string | `"auth.deepcypher.me"` |  |
| keycloak.ingress.tls | bool | `true` |  |
| keycloak.livenessProbe.enabled | bool | `true` |  |
| keycloak.postgresql.auth.existingSecret | string | `"postgres"` |  |
| keycloak.readinessProbe.enabled | bool | `true` |  |
| keycloak.resources.limits.memory | string | `"2Gi"` |  |
| keycloak.resources.requests.cpu | string | `"500m"` |  |
| keycloak.startupProbe.enabled | bool | `true` |  |

