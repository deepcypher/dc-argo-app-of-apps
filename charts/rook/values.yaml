rook-ceph:
  enableDiscoveryDaemon: true

  monitoring:
    enabled: true

rook-ceph-cluster:

  monitoring:
    enabled: true
    createPrometheusRules: true

  configOverride: |
    [global]
    mon_allow_pool_delete = true
    osd_pool_default_size = 2
    osd_pool_default_min_size = 0 # if 0 then min_size = size - (size / 2)


  # GENERIC
  enabled: true
  toolbox:
    enabled: true

  # CEPH CLUSTER SPEC
  cephClusterSpec:
    cleanupPolicy:
      confirmation: ""
      #confirmation: "yes-really-destroy-data"
      sanitizeDisks:
        method: "quick"
    dataDirHostPath: /var/lib/rook
    dashboard:
      enabled: true
    mgr:
      allowMultiplePerNode: false
      modules:
      # enabling visualization of physical disks using rook manager orchestrator backend
      - name: rook
        enabled: true
      # enabling volume and subvolume commands like `fs subvolume info` necessary for velero
      - name: volumes
        enabled: true
    mon:
      allowMultiplePerNode: false
    storage:
      useAllNodes: false
      useAllDevices: false
      deviceFilter:
      nodes:
      - name: "honey"
        devices:
        - name: "/dev/data/data"
      - name: "nectar"
        devices:
        - name: "/dev/data/data"
    # setting limits for resources
    # https://github.com/rook/rook/blob/master/design/ceph/resource-constraints.md#cluster-crd
    resources:
      api:
        requests:
          cpu: "500m"
          memory: "512Mi"
        limits:
          #cpu: "500m"
          memory: "512Mi"
      mon:
        requests:
          cpu: "400m"
          memory: "1024Mi"
        limits:
          #cpu: "500m"
          memory: "1024Mi"
      mgr:
        requests:
          cpu: "500m"
          #memory: "1024Mi"
        limits:
          #cpu: "500m"
          memory: "2048Mi"
      osd:
        requests:
          cpu: "500m"
          memory: "3072Mi"
        limits:
          #cpu: "500m"
          memory: "3072Mi"
      crashcollector:
        requests:
          cpu: "50m"
          memory: "60Mi"
        limits:
          #cpu: "500m"
          memory: "60Mi"
      logcollector:
        limits:
          memory: "1024Mi"
        requests:
          cpu: "100m"
          memory: "1024Mi"
      cleanup:
        limits:
          memory: "1Gi"
        requests:
          cpu: "500m"
          memory: "100Mi"
      mgr-sidecar:
        requests:
          cpu: "100m"
          memory: "40Mi"
        limits:
          #cpu: "500m"
          memory: "100Mi"
      prepareosd:
        requests:
          cpu: "500m"
          memory: "2048Mi"
        #limits:
          #cpu: "500m"
          #memory: "8192Mi"
      exporter:
        requests:
          cpu: "100m"
          memory: "50Mi"
        limits:
          #cpu: "500m"
          memory: "128Mi"

  # BLOCK SPEC
  cephBlockPools:
  - name: ceph-blockpool
    # see https://github.com/rook/rook/blob/master/Documentation/CRDs/Block-Storage/ceph-block-pool-crd.md#spec for available configuration
    spec:
      failureDomain: host
      replicated:
        # size: 3
        size: 2
      # Enables collecting RBD per-image IO statistics by enabling dynamic OSD performance counters. Defaults to false.
      # For reference: https://docs.ceph.com/docs/master/mgr/prometheus/#rbd-io-statistics
      # enableRBDStats: true
    storageClass:
      enabled: true
      name: ceph-block
      # isDefault: true
      isDefault: false
      reclaimPolicy: Delete
      allowVolumeExpansion: true
      volumeBindingMode: "Immediate"
      mountOptions: []
      # see https://kubernetes.io/docs/concepts/storage/storage-classes/#allowed-topologies
      allowedTopologies: []
#        - matchLabelExpressions:
#            - key: rook-ceph-role
#              values:
#                - storage-node
      # see https://github.com/rook/rook/blob/master/Documentation/ceph-block.md#provision-storage for available configuration
      parameters:
        # (optional) mapOptions is a comma-separated list of map options.
        # For krbd options refer
        # https://docs.ceph.com/docs/master/man/8/rbd/#kernel-rbd-krbd-options
        # For nbd options refer
        # https://docs.ceph.com/docs/master/man/8/rbd-nbd/#options
        # mapOptions: lock_on_read,queue_depth=1024

        # (optional) unmapOptions is a comma-separated list of unmap options.
        # For krbd options refer
        # https://docs.ceph.com/docs/master/man/8/rbd/#kernel-rbd-krbd-options
        # For nbd options refer
        # https://docs.ceph.com/docs/master/man/8/rbd-nbd/#options
        # unmapOptions: force

        # RBD image format. Defaults to "2".
        imageFormat: "2"

        # RBD image features, equivalent to OR'd bitfield value: 63
        # Available for imageFormat: "2". Older releases of CSI RBD
        # support only the `layering` feature. The Linux kernel (KRBD) supports the
        # full feature complement as of 5.4
        imageFeatures: layering

        # These secrets contain Ceph admin credentials.
        csi.storage.k8s.io/provisioner-secret-name: rook-csi-rbd-provisioner
        csi.storage.k8s.io/provisioner-secret-namespace: "{{ .Release.Namespace }}"
        csi.storage.k8s.io/controller-expand-secret-name: rook-csi-rbd-provisioner
        csi.storage.k8s.io/controller-expand-secret-namespace: "{{ .Release.Namespace }}"
        csi.storage.k8s.io/node-stage-secret-name: rook-csi-rbd-node
        csi.storage.k8s.io/node-stage-secret-namespace: "{{ .Release.Namespace }}"
        # Specify the filesystem type of the volume. If not specified, csi-provisioner
        # will set default as `ext4`. Note that `xfs` is not recommended due to potential deadlock
        # in hyperconverged settings where the volume is mounted on the same node as the osds.
        csi.storage.k8s.io/fstype: ext4

  # -- Settings for the block pool snapshot class
  # @default -- See [RBD Snapshots](../Storage-Configuration/Ceph-CSI/ceph-csi-snapshot.md#rbd-snapshots)
  cephBlockPoolsVolumeSnapshotClass:
    enabled: true
    name: ceph-block
    isDefault: true
    deletionPolicy: Delete
    annotations: {}
    labels:
      velero.io/csi-volumesnapshot-class: "true"
    # see https://rook.io/docs/rook/v1.10/Storage-Configuration/Ceph-CSI/ceph-csi-snapshot/#rbd-snapshots for available configuration
    parameters: {}

  # FILESYSTEM SPEC
  cephFileSystems:
  - name: ceph-filesystem
    # see https://github.com/rook/rook/blob/master/Documentation/CRDs/Shared-Filesystem/ceph-filesystem-crd.md#filesystem-settings for available configuration
    spec:
      metadataPool:
        replicated:
          # size: 3
          size: 2
          #requireSafeReplicaSize: false # allows pool size of 1
          requireSafeReplicaSize: true
      dataPools:
        - failureDomain: host
          replicated:
            size: 2
          # Optional and highly recommended, 'data0' by default, see https://github.com/rook/rook/blob/master/Documentation/CRDs/Shared-Filesystem/ceph-filesystem-crd.md#pools
          name: data0
      metadataServer:
        activeCount: 1
        activeStandby: true
        resources:
          limits:
            cpu: "2000m"
            memory: "7Gi"
          requests:
            cpu: "1000m"
            memory: "7Gi"
        priorityClassName: system-cluster-critical
    storageClass:
      enabled: true
      # isDefault: false
      isDefault: true
      name: ceph-filesystem
      # (Optional) specify a data pool to use, must be the name of one of the data pools above, 'data0' by default
      pool: data0
      reclaimPolicy: Delete
      allowVolumeExpansion: true
      volumeBindingMode: "Immediate"
      mountOptions: []
      # see https://github.com/rook/rook/blob/master/Documentation/ceph-filesystem.md#provision-storage for available configuration
      parameters:
        # The secrets contain Ceph admin credentials.
        csi.storage.k8s.io/provisioner-secret-name: rook-csi-cephfs-provisioner
        csi.storage.k8s.io/provisioner-secret-namespace: "{{ .Release.Namespace }}"
        csi.storage.k8s.io/controller-expand-secret-name: rook-csi-cephfs-provisioner
        csi.storage.k8s.io/controller-expand-secret-namespace: "{{ .Release.Namespace }}"
        csi.storage.k8s.io/node-stage-secret-name: rook-csi-cephfs-node
        csi.storage.k8s.io/node-stage-secret-namespace: "{{ .Release.Namespace }}"
        # Specify the filesystem type of the volume. If not specified, csi-provisioner
        # will set default as `ext4`. Note that `xfs` is not recommended due to potential deadlock
        # in hyperconverged settings where the volume is mounted on the same node as the osds.
        csi.storage.k8s.io/fstype: ext4

  # -- Settings for the filesystem snapshot class
  # @default -- See [CephFS Snapshots](../Storage-Configuration/Ceph-CSI/ceph-csi-snapshot.md#cephfs-snapshots)
  cephFileSystemVolumeSnapshotClass:
    enabled: true
    name: ceph-filesystem
    isDefault: true
    deletionPolicy: Delete
    annotations: {}
    labels:
      velero.io/csi-volumesnapshot-class: "true"
    # see https://rook.io/docs/rook/v1.10/Storage-Configuration/Ceph-CSI/ceph-csi-snapshot/#cephfs-snapshots for available configuration
    parameters: {}

  # OBJECT STORE SPEC
  # https://github.com/rook/rook/blob/08ec3a6e52b2fb3b4e89a9b4283de620a76c22c7/deploy/charts/rook-ceph-cluster/values.yaml#L558-L595
  cephObjectStores:
  - name: ceph-objectstore
    # see https://github.com/rook/rook/blob/master/Documentation/CRDs/Object-Storage/ceph-object-store-crd.md#object-store-settings for available configuration
    spec:
      metadataPool:
        # failureDomain: host
        failureDomain: host
        replicated:
          # size: 3
          size: 2
          #requireSafeReplicaSize: false
          requireSafeReplicaSize: true
      dataPool:
        failureDomain: host
        erasureCoded:
          dataChunks: 2
          codingChunks: 1
      preservePoolsOnDelete: true
      gateway:
        port: 80
        resources:
          limits:
            cpu: "2000m"
            memory: "2Gi"
          requests:
            cpu: "1000m"
            memory: "1Gi"
        # securePort: 443
        # sslCertificateRef:
        instances: 1
        priorityClassName: system-cluster-critical
    storageClass:
      enabled: true
      name: ceph-bucket
      reclaimPolicy: Delete
      volumeBindingMode: "Immediate"
      # isDefault: false
      # see https://github.com/rook/rook/blob/master/Documentation/ceph-object-bucket-claim.md#storageclass for available configuration
      parameters:
        # note: objectStoreNamespace and objectStoreName are configured by the chart
        region: us-east-1
