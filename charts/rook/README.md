# rook

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.rook.io/release | rook-ceph | v1.15.6 |
| https://charts.rook.io/release | rook-ceph-cluster | v1.15.6 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| rook-ceph-cluster.cephBlockPoolsVolumeSnapshotClass | object | See [RBD Snapshots](../Storage-Configuration/Ceph-CSI/ceph-csi-snapshot.md#rbd-snapshots) | Settings for the block pool snapshot class |
| rook-ceph-cluster.cephBlockPools[0].name | string | `"ceph-blockpool"` |  |
| rook-ceph-cluster.cephBlockPools[0].spec.failureDomain | string | `"host"` |  |
| rook-ceph-cluster.cephBlockPools[0].spec.replicated.size | int | `2` |  |
| rook-ceph-cluster.cephBlockPools[0].storageClass.allowVolumeExpansion | bool | `true` |  |
| rook-ceph-cluster.cephBlockPools[0].storageClass.allowedTopologies | list | `[]` |  |
| rook-ceph-cluster.cephBlockPools[0].storageClass.enabled | bool | `true` |  |
| rook-ceph-cluster.cephBlockPools[0].storageClass.isDefault | bool | `false` |  |
| rook-ceph-cluster.cephBlockPools[0].storageClass.mountOptions | list | `[]` |  |
| rook-ceph-cluster.cephBlockPools[0].storageClass.name | string | `"ceph-block"` |  |
| rook-ceph-cluster.cephBlockPools[0].storageClass.parameters."csi.storage.k8s.io/controller-expand-secret-name" | string | `"rook-csi-rbd-provisioner"` |  |
| rook-ceph-cluster.cephBlockPools[0].storageClass.parameters."csi.storage.k8s.io/controller-expand-secret-namespace" | string | `"{{ .Release.Namespace }}"` |  |
| rook-ceph-cluster.cephBlockPools[0].storageClass.parameters."csi.storage.k8s.io/fstype" | string | `"ext4"` |  |
| rook-ceph-cluster.cephBlockPools[0].storageClass.parameters."csi.storage.k8s.io/node-stage-secret-name" | string | `"rook-csi-rbd-node"` |  |
| rook-ceph-cluster.cephBlockPools[0].storageClass.parameters."csi.storage.k8s.io/node-stage-secret-namespace" | string | `"{{ .Release.Namespace }}"` |  |
| rook-ceph-cluster.cephBlockPools[0].storageClass.parameters."csi.storage.k8s.io/provisioner-secret-name" | string | `"rook-csi-rbd-provisioner"` |  |
| rook-ceph-cluster.cephBlockPools[0].storageClass.parameters."csi.storage.k8s.io/provisioner-secret-namespace" | string | `"{{ .Release.Namespace }}"` |  |
| rook-ceph-cluster.cephBlockPools[0].storageClass.parameters.imageFeatures | string | `"layering"` |  |
| rook-ceph-cluster.cephBlockPools[0].storageClass.parameters.imageFormat | string | `"2"` |  |
| rook-ceph-cluster.cephBlockPools[0].storageClass.reclaimPolicy | string | `"Delete"` |  |
| rook-ceph-cluster.cephBlockPools[0].storageClass.volumeBindingMode | string | `"Immediate"` |  |
| rook-ceph-cluster.cephClusterSpec.cleanupPolicy.confirmation | string | `""` |  |
| rook-ceph-cluster.cephClusterSpec.cleanupPolicy.sanitizeDisks.method | string | `"quick"` |  |
| rook-ceph-cluster.cephClusterSpec.dashboard.enabled | bool | `true` |  |
| rook-ceph-cluster.cephClusterSpec.dataDirHostPath | string | `"/var/lib/rook"` |  |
| rook-ceph-cluster.cephClusterSpec.mgr.allowMultiplePerNode | bool | `false` |  |
| rook-ceph-cluster.cephClusterSpec.mgr.modules[0].enabled | bool | `true` |  |
| rook-ceph-cluster.cephClusterSpec.mgr.modules[0].name | string | `"rook"` |  |
| rook-ceph-cluster.cephClusterSpec.mgr.modules[1].enabled | bool | `true` |  |
| rook-ceph-cluster.cephClusterSpec.mgr.modules[1].name | string | `"volumes"` |  |
| rook-ceph-cluster.cephClusterSpec.mon.allowMultiplePerNode | bool | `false` |  |
| rook-ceph-cluster.cephClusterSpec.resources.api.limits.memory | string | `"512Mi"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.api.requests.cpu | string | `"500m"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.api.requests.memory | string | `"512Mi"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.cleanup.limits.memory | string | `"1Gi"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.cleanup.requests.cpu | string | `"500m"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.cleanup.requests.memory | string | `"100Mi"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.crashcollector.limits.memory | string | `"60Mi"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.crashcollector.requests.cpu | string | `"50m"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.crashcollector.requests.memory | string | `"60Mi"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.exporter.limits.memory | string | `"128Mi"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.exporter.requests.cpu | string | `"100m"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.exporter.requests.memory | string | `"50Mi"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.logcollector.limits.memory | string | `"1024Mi"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.logcollector.requests.cpu | string | `"100m"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.logcollector.requests.memory | string | `"1024Mi"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.mgr-sidecar.limits.memory | string | `"100Mi"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.mgr-sidecar.requests.cpu | string | `"100m"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.mgr-sidecar.requests.memory | string | `"40Mi"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.mgr.limits.memory | string | `"2048Mi"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.mgr.requests.cpu | string | `"500m"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.mon.limits.memory | string | `"1024Mi"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.mon.requests.cpu | string | `"400m"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.mon.requests.memory | string | `"1024Mi"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.osd.limits.memory | string | `"3072Mi"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.osd.requests.cpu | string | `"500m"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.osd.requests.memory | string | `"3072Mi"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.prepareosd.requests.cpu | string | `"500m"` |  |
| rook-ceph-cluster.cephClusterSpec.resources.prepareosd.requests.memory | string | `"2048Mi"` |  |
| rook-ceph-cluster.cephClusterSpec.storage.deviceFilter | string | `nil` |  |
| rook-ceph-cluster.cephClusterSpec.storage.nodes[0].devices[0].name | string | `"/dev/data/data"` |  |
| rook-ceph-cluster.cephClusterSpec.storage.nodes[0].name | string | `"honey"` |  |
| rook-ceph-cluster.cephClusterSpec.storage.nodes[1].devices[0].name | string | `"/dev/data/data"` |  |
| rook-ceph-cluster.cephClusterSpec.storage.nodes[1].name | string | `"nectar"` |  |
| rook-ceph-cluster.cephClusterSpec.storage.useAllDevices | bool | `false` |  |
| rook-ceph-cluster.cephClusterSpec.storage.useAllNodes | bool | `false` |  |
| rook-ceph-cluster.cephFileSystemVolumeSnapshotClass | object | See [CephFS Snapshots](../Storage-Configuration/Ceph-CSI/ceph-csi-snapshot.md#cephfs-snapshots) | Settings for the filesystem snapshot class |
| rook-ceph-cluster.cephFileSystems[0].name | string | `"ceph-filesystem"` |  |
| rook-ceph-cluster.cephFileSystems[0].spec.dataPools[0].failureDomain | string | `"host"` |  |
| rook-ceph-cluster.cephFileSystems[0].spec.dataPools[0].name | string | `"data0"` |  |
| rook-ceph-cluster.cephFileSystems[0].spec.dataPools[0].replicated.size | int | `2` |  |
| rook-ceph-cluster.cephFileSystems[0].spec.metadataPool.replicated.requireSafeReplicaSize | bool | `true` |  |
| rook-ceph-cluster.cephFileSystems[0].spec.metadataPool.replicated.size | int | `2` |  |
| rook-ceph-cluster.cephFileSystems[0].spec.metadataServer.activeCount | int | `1` |  |
| rook-ceph-cluster.cephFileSystems[0].spec.metadataServer.activeStandby | bool | `true` |  |
| rook-ceph-cluster.cephFileSystems[0].spec.metadataServer.priorityClassName | string | `"system-cluster-critical"` |  |
| rook-ceph-cluster.cephFileSystems[0].spec.metadataServer.resources.limits.cpu | string | `"2000m"` |  |
| rook-ceph-cluster.cephFileSystems[0].spec.metadataServer.resources.limits.memory | string | `"7Gi"` |  |
| rook-ceph-cluster.cephFileSystems[0].spec.metadataServer.resources.requests.cpu | string | `"1000m"` |  |
| rook-ceph-cluster.cephFileSystems[0].spec.metadataServer.resources.requests.memory | string | `"7Gi"` |  |
| rook-ceph-cluster.cephFileSystems[0].storageClass.allowVolumeExpansion | bool | `true` |  |
| rook-ceph-cluster.cephFileSystems[0].storageClass.enabled | bool | `true` |  |
| rook-ceph-cluster.cephFileSystems[0].storageClass.isDefault | bool | `true` |  |
| rook-ceph-cluster.cephFileSystems[0].storageClass.mountOptions | list | `[]` |  |
| rook-ceph-cluster.cephFileSystems[0].storageClass.name | string | `"ceph-filesystem"` |  |
| rook-ceph-cluster.cephFileSystems[0].storageClass.parameters."csi.storage.k8s.io/controller-expand-secret-name" | string | `"rook-csi-cephfs-provisioner"` |  |
| rook-ceph-cluster.cephFileSystems[0].storageClass.parameters."csi.storage.k8s.io/controller-expand-secret-namespace" | string | `"{{ .Release.Namespace }}"` |  |
| rook-ceph-cluster.cephFileSystems[0].storageClass.parameters."csi.storage.k8s.io/fstype" | string | `"ext4"` |  |
| rook-ceph-cluster.cephFileSystems[0].storageClass.parameters."csi.storage.k8s.io/node-stage-secret-name" | string | `"rook-csi-cephfs-node"` |  |
| rook-ceph-cluster.cephFileSystems[0].storageClass.parameters."csi.storage.k8s.io/node-stage-secret-namespace" | string | `"{{ .Release.Namespace }}"` |  |
| rook-ceph-cluster.cephFileSystems[0].storageClass.parameters."csi.storage.k8s.io/provisioner-secret-name" | string | `"rook-csi-cephfs-provisioner"` |  |
| rook-ceph-cluster.cephFileSystems[0].storageClass.parameters."csi.storage.k8s.io/provisioner-secret-namespace" | string | `"{{ .Release.Namespace }}"` |  |
| rook-ceph-cluster.cephFileSystems[0].storageClass.pool | string | `"data0"` |  |
| rook-ceph-cluster.cephFileSystems[0].storageClass.reclaimPolicy | string | `"Delete"` |  |
| rook-ceph-cluster.cephFileSystems[0].storageClass.volumeBindingMode | string | `"Immediate"` |  |
| rook-ceph-cluster.cephObjectStores[0].name | string | `"ceph-objectstore"` |  |
| rook-ceph-cluster.cephObjectStores[0].spec.dataPool.erasureCoded.codingChunks | int | `1` |  |
| rook-ceph-cluster.cephObjectStores[0].spec.dataPool.erasureCoded.dataChunks | int | `2` |  |
| rook-ceph-cluster.cephObjectStores[0].spec.dataPool.failureDomain | string | `"host"` |  |
| rook-ceph-cluster.cephObjectStores[0].spec.gateway.instances | int | `1` |  |
| rook-ceph-cluster.cephObjectStores[0].spec.gateway.port | int | `80` |  |
| rook-ceph-cluster.cephObjectStores[0].spec.gateway.priorityClassName | string | `"system-cluster-critical"` |  |
| rook-ceph-cluster.cephObjectStores[0].spec.gateway.resources.limits.cpu | string | `"2000m"` |  |
| rook-ceph-cluster.cephObjectStores[0].spec.gateway.resources.limits.memory | string | `"2Gi"` |  |
| rook-ceph-cluster.cephObjectStores[0].spec.gateway.resources.requests.cpu | string | `"1000m"` |  |
| rook-ceph-cluster.cephObjectStores[0].spec.gateway.resources.requests.memory | string | `"1Gi"` |  |
| rook-ceph-cluster.cephObjectStores[0].spec.metadataPool.failureDomain | string | `"host"` |  |
| rook-ceph-cluster.cephObjectStores[0].spec.metadataPool.replicated.requireSafeReplicaSize | bool | `true` |  |
| rook-ceph-cluster.cephObjectStores[0].spec.metadataPool.replicated.size | int | `2` |  |
| rook-ceph-cluster.cephObjectStores[0].spec.preservePoolsOnDelete | bool | `true` |  |
| rook-ceph-cluster.cephObjectStores[0].storageClass.enabled | bool | `true` |  |
| rook-ceph-cluster.cephObjectStores[0].storageClass.name | string | `"ceph-bucket"` |  |
| rook-ceph-cluster.cephObjectStores[0].storageClass.parameters.region | string | `"us-east-1"` |  |
| rook-ceph-cluster.cephObjectStores[0].storageClass.reclaimPolicy | string | `"Delete"` |  |
| rook-ceph-cluster.cephObjectStores[0].storageClass.volumeBindingMode | string | `"Immediate"` |  |
| rook-ceph-cluster.configOverride | string | `"[global]\nmon_allow_pool_delete = true\nosd_pool_default_size = 2\nosd_pool_default_min_size = 0 # if 0 then min_size = size - (size / 2)\n"` |  |
| rook-ceph-cluster.enabled | bool | `true` |  |
| rook-ceph-cluster.monitoring.createPrometheusRules | bool | `true` |  |
| rook-ceph-cluster.monitoring.enabled | bool | `true` |  |
| rook-ceph-cluster.toolbox.enabled | bool | `true` |  |
| rook-ceph.enableDiscoveryDaemon | bool | `true` |  |
| rook-ceph.monitoring.enabled | bool | `true` |  |

