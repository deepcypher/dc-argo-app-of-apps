# argo-workflows

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: v3.6.4](https://img.shields.io/badge/AppVersion-v3.6.4-informational?style=flat-square)

Argo workflows for kubernetes pipelines

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://argoproj.github.io/argo-helm | argo-workflows | 0.45.7 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| argo-workflows.server.authModes[0] | string | `"sso"` |  |
| argo-workflows.server.ingress.annotations."cert-manager.io/cluster-issuer" | string | `"aux-issuer"` |  |
| argo-workflows.server.ingress.annotations."traefik.ingress.kubernetes.io/router.middlewares" | string | `"traefik-headers@kubernetescrd"` |  |
| argo-workflows.server.ingress.enabled | bool | `true` |  |
| argo-workflows.server.ingress.hosts[0] | string | `"workflows.deepcypher.me"` |  |
| argo-workflows.server.ingress.tls[0].hosts[0] | string | `"workflows.deepcypher.me"` |  |
| argo-workflows.server.ingress.tls[0].secretName | string | `"workflows.deepcypher.me-tls"` |  |
| argo-workflows.server.logging.level | string | `"warn"` |  |
| argo-workflows.server.sso.clientId.key | string | `"clientId"` |  |
| argo-workflows.server.sso.clientId.name | string | `"oidc-credentials"` |  |
| argo-workflows.server.sso.clientSecret.key | string | `"clientSecret"` |  |
| argo-workflows.server.sso.clientSecret.name | string | `"oidc-credentials"` |  |
| argo-workflows.server.sso.enabled | bool | `true` |  |
| argo-workflows.server.sso.issuer | string | `"https://auth.deepcypher.me/realms/deepcypher"` |  |
| argo-workflows.server.sso.redirectUrl | string | `"https://workflows.deepcypher.me/oauth2/callback"` |  |
| argo-workflows.server.sso.scopes[0] | string | `"openid"` |  |
| argo-workflows.server.sso.scopes[1] | string | `"email"` |  |
| argo-workflows.server.sso.scopes[2] | string | `"profile"` |  |
| argo-workflows.server.sso.scopes[3] | string | `"roles"` |  |
| argo-workflows.server.sso.scopes[4] | string | `"groups"` |  |
| argo-workflows.server.sso.sessionExpiry | string | `"24h"` |  |
| environment.baseDomain | string | `"deepcypher.me"` |  |
| environment.contact.email | string | `"noreply@deepcypher.me"` |  |
| environment.contact.name | string | `"George Onoufriou"` |  |
| environment.hardware | string | `"metal"` |  |
| environment.location.name | string | `"unknown"` |  |
| environment.mode | string | `"production"` |  |
| environment.name | string | `"unknown"` |  |
| environment.revision | string | `"main"` |  |
| oidc.realm | string | `"deepcypher"` |  |

