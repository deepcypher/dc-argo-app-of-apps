# foundryvtt

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.0.25](https://img.shields.io/badge/AppVersion-0.0.25-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://gitlab.com/api/v4/projects/55284972/packages/helm/stable | ddb-proxy | 0.1.1 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| ddb-proxy.command | string | `nil` |  |
| ddb-proxy.ingress.annotations."cert-manager.io/cluster-issuer" | string | `"aux-issuer"` |  |
| ddb-proxy.ingress.annotations."traefik.ingress.kubernetes.io/router.middlewares" | string | `"traefik-headers@kubernetescrd"` |  |
| ddb-proxy.ingress.className | string | `"traefik"` |  |
| ddb-proxy.ingress.enabled | bool | `true` |  |
| ddb-proxy.ingress.hosts[0].host | string | `"ddb-proxy.deepcypher.me"` |  |
| ddb-proxy.ingress.hosts[0].paths[0].path | string | `"/"` |  |
| ddb-proxy.ingress.hosts[0].paths[0].pathType | string | `"ImplementationSpecific"` |  |
| ddb-proxy.ingress.tls[0].hosts[0] | string | `"ddb-proxy.deepcypher.me"` |  |
| ddb-proxy.ingress.tls[0].secretName | string | `"ddb-proxy.deepcypher.me-tls"` |  |
| ddb-proxy.netpol.enabled | bool | `true` |  |
| ddb-proxy.resources.limits.memory | string | `"1Gi"` |  |
| ddb-proxy.resources.requests.cpu | string | `"200m"` |  |
| ddb-proxy.securityContext.capabilities.drop[0] | string | `"ALL"` |  |
| ddb-proxy.securityContext.readOnlyRootFilesystem | bool | `false` |  |

