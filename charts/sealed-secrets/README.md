# sealed-secrets

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.bitnami.com/bitnami | sealed-secrets | 2.5.6 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| sealed-secrets.createController | bool | `true` |  |
| sealed-secrets.ingress.enabled | bool | `false` |  |
| sealed-secrets.ingress.hostname | string | `"secrets.deepcypher.me"` |  |
| sealed-secrets.networkPolicy.enabled | bool | `true` |  |

