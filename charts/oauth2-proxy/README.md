# sealed-secrets

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://oauth2-proxy.github.io/manifests | oauth2-proxy | 7.11.1 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| environment.baseDomain | string | `"deepcypher.me"` |  |
| environment.contact.email | string | `"noreply@deepcypher.me"` |  |
| environment.contact.name | string | `"George Onoufriou"` |  |
| environment.hardware | string | `"metal"` |  |
| environment.location.name | string | `"unknown"` |  |
| environment.mode | string | `"production"` |  |
| environment.name | string | `"unknown"` |  |
| environment.revision | string | `"main"` |  |
| oauth2-proxy.autoscaling.enabled | bool | `true` |  |
| oauth2-proxy.autoscaling.maxReplicas | int | `3` |  |
| oauth2-proxy.autoscaling.minReplicas | int | `1` |  |
| oauth2-proxy.config.configFile | string | `"provider = \"keycloak-oidc\"\nredirect_url = \"https://oauth2-proxy.deepcypher.me/oauth2/callback\"\noidc_issuer_url = \"https://auth.deepcypher.me/realms/deepcypher\"\ncode_challenge_method = \"S256\"\nemail_domains=[\"*\"]\nupstreams = [\"static://200\"]"` |  |
| oauth2-proxy.config.existingSecret | string | `"oidc-credentials"` |  |
| oauth2-proxy.ingress.annotations."cert-manager.io/cluster-issuer" | string | `"aux-issuer"` |  |
| oauth2-proxy.ingress.annotations."traefik.ingress.kubernetes.io/router.middlewares" | string | `"traefik-headers@kubernetescrd"` |  |
| oauth2-proxy.ingress.className | string | `"traefik"` |  |
| oauth2-proxy.ingress.enabled | bool | `true` |  |
| oauth2-proxy.ingress.hosts[0] | string | `"oauth2-proxy.deepcypher.me"` |  |
| oauth2-proxy.ingress.labels | object | `{}` |  |
| oauth2-proxy.ingress.path | string | `"/"` |  |
| oauth2-proxy.ingress.pathType | string | `"ImplementationSpecific"` |  |
| oauth2-proxy.ingress.tls[0].hosts[0] | string | `"oauth2-proxy.deepcypher.me"` |  |
| oauth2-proxy.ingress.tls[0].secretName | string | `"oauth2-proxy.deepcypher.me-tls"` |  |
| oauth2-proxy.initContainers.waitForRedis.enabled | bool | `false` |  |
| oauth2-proxy.metrics.enabled | bool | `true` |  |
| oauth2-proxy.metrics.serviceMonitor.enabled | bool | `true` |  |
| oauth2-proxy.redis.auth.enabled | bool | `true` |  |
| oauth2-proxy.redis.auth.existingSecret | string | `"redis-creds"` |  |
| oauth2-proxy.redis.auth.existingSecretPasswordKey | string | `"redis-password"` |  |
| oauth2-proxy.redis.auth.sentinel | bool | `false` |  |
| oauth2-proxy.redis.enabled | bool | `false` |  |
| oauth2-proxy.resources.limits.memory | string | `"100Mi"` |  |
| oauth2-proxy.resources.requests.cpu | string | `"60m"` |  |
| oidc.realm | string | `"deepcypher"` |  |

