# metrics

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 4](https://img.shields.io/badge/AppVersion-4-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://operator.min.io | tenant | 6.0.4 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| environment.baseDomain | string | `"deepcypher.me"` |  |
| environment.contact.email | string | `"noreply@deepcypher.me"` |  |
| environment.contact.name | string | `"George Onoufriou"` |  |
| environment.hardware | string | `"metal"` |  |
| environment.location.name | string | `"unknown"` |  |
| environment.mode | string | `"production"` |  |
| environment.name | string | `"unknown"` |  |
| environment.revision | string | `"main"` |  |
| tenant.enabled | bool | `true` | - I keep forgetting so as a note to myself DO NOT INSERT ANYTHING HERE this is not part of the tenant chart |
| tenant.ingress.api.annotations."cert-manager.io/cluster-issuer" | string | `"aux-issuer"` |  |
| tenant.ingress.api.enabled | bool | `false` |  |
| tenant.ingress.api.host | string | `"s3.deepcypher.me"` |  |
| tenant.ingress.api.tls[0].hosts[0] | string | `"s3.deepcypher.me"` |  |
| tenant.ingress.api.tls[0].secretName | string | `"s3.deepcypher.me-tls"` |  |
| tenant.ingress.console.annotations."cert-manager.io/cluster-issuer" | string | `"aux-issuer"` |  |
| tenant.ingress.console.enabled | bool | `false` |  |
| tenant.ingress.console.host | string | `"minio.deepcypher.me"` |  |
| tenant.ingress.console.tls[0].hosts[0] | string | `"minio.deepcypher.me"` |  |
| tenant.ingress.console.tls[0].secretName | string | `"minio.deepcypher.me-tls"` |  |
| tenant.tenant | object | `{"buckets":[{"name":"loki","objectLock":false,"region":"eu-west-2"},{"name":"tempo","objectLock":false,"region":"eu-west-2"},{"name":"mimir","objectLock":false,"region":"eu-west-2"},{"name":"prometheus","objectLock":false,"region":"eu-west-2"}],"certificate":{"requestAutoCert":false},"configSecret":{"accessKey":"","existingSecret":true,"name":"minio","secretKey":""},"configuration":{"name":"minio"},"image":{"pullPolicy":"IfNotPresent","repository":"quay.io/minio/minio","tag":"RELEASE.2024-10-13T13-34-11Z"},"metrics":{"enabled":true,"port":9000,"protocol":"http"},"name":"metrics","pools":[{"containerSecurityContext":{"allowPrivilegeEscalation":false,"capabilities":{"drop":["ALL"]},"runAsGroup":1000,"runAsNonRoot":true,"runAsUser":1000,"seccompProfile":{"type":"RuntimeDefault"}},"name":"pool-0","resources":{"limits":{"memory":"2Gi"},"requests":{"cpu":"500m","ephemeral-storage":"10Gi"}},"securityContext":{"fsGroup":1000,"fsGroupChangePolicy":"OnRootMismatch","runAsGroup":1000,"runAsNonRoot":true,"runAsUser":1000},"servers":4,"size":"50Gi","volumesPerServer":1}],"users":[{"name":"loki"},{"name":"tempo"},{"name":"mimir"},{"name":"prometheus"}]}` | - |

