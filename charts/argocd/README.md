# argo-cd

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://argoproj.github.io/argo-helm/ | argo-cd | 7.8.5 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| argo-cd.applicationSet.replicas | int | `2` |  |
| argo-cd.applicationSet.resources.limits.memory | string | `"1Gi"` |  |
| argo-cd.applicationSet.resources.requests.cpu | string | `"600m"` |  |
| argo-cd.configs.cm."resource.exclusions" | string | `"- apiGroups:\n  - cilium.io\n  kinds:\n  - CiliumIdentity\n  clusters:\n  - \"*\"\n"` |  |
| argo-cd.configs.cm.admin.enabled | bool | `true` |  |
| argo-cd.configs.cm.create | bool | `true` |  |
| argo-cd.configs.cm.dex.config | string | `"connectors:\n# GitHub example\n- type: OIDC\n  id: authentik\n  name: Authentik\n  config:\n    issuer: https://auth.deepcypher.me\n    clientID: $oidc:clientID\n    clientSecret: $oidc:clientSecret # Alternatively $<some_K8S_secret>:dex.github.clientSecret\n    # to enable fetching groups claim from provider that is not already in the token\n    # apiVersion: v1\n    # kind: Secret\n    # metadata:\n    #   name: oidc\n    #   namespace: argocd\n    #   labels:\n    #     app.kubernetes.io/part-of: argocd # THIS MUST BE LABELED THUS to be picked up by argocd\n    # type: Opaque\n    # data:\n    #   clientID: <base64 encoded clientID>\n    #   clientSecret: <base64 encoded clientSecret>\n    insecureEnableGroups: true\n    getUserInfo: true\n    scopes:\n    - profile\n    - email\n    - groups\n"` |  |
| argo-cd.configs.params."server.insecure" | bool | `true` |  |
| argo-cd.controller.replicas | int | `2` |  |
| argo-cd.controller.resources.limits.memory | string | `"2Gi"` |  |
| argo-cd.controller.resources.requests.cpu | string | `"900m"` |  |
| argo-cd.dex.enabled | bool | `false` |  |
| argo-cd.dex.resources.limits.memory | string | `"256Mi"` |  |
| argo-cd.dex.resources.requests.cpu | string | `"500m"` |  |
| argo-cd.global.domain | string | `"argocd.deepcypher.me"` |  |
| argo-cd.global.priorityClassName | string | `"system-cluster-critical"` |  |
| argo-cd.redis-ha.enabled | bool | `true` |  |
| argo-cd.redis-ha.haproxy.resources.requests.cpu | string | `"100m"` |  |
| argo-cd.redis-ha.haproxy.resources.requests.memory | string | `"100Mi"` |  |
| argo-cd.redis-ha.redis.resources.requests.cpu | string | `"100m"` |  |
| argo-cd.redis-ha.redis.resources.requests.memory | string | `"100Mi"` |  |
| argo-cd.repoServer.autoscaling.enabled | bool | `true` |  |
| argo-cd.repoServer.autoscaling.maxReplicas | int | `3` |  |
| argo-cd.repoServer.autoscaling.minReplicas | int | `2` |  |
| argo-cd.repoServer.resources.limits.memory | string | `"900Mi"` |  |
| argo-cd.repoServer.resources.requests.cpu | string | `"600m"` |  |
| argo-cd.server.autoscaling.enabled | bool | `true` |  |
| argo-cd.server.autoscaling.maxReplicas | int | `3` |  |
| argo-cd.server.autoscaling.minReplicas | int | `2` |  |
| argo-cd.server.ingress.annotations."cert-manager.io/cluster-issuer" | string | `"aux-issuer"` |  |
| argo-cd.server.ingress.enabled | bool | `true` |  |
| argo-cd.server.ingress.extraTls[0].hosts[0] | string | `"argocd.deepcypher.me"` |  |
| argo-cd.server.ingress.extraTls[0].secretName | string | `"argocd-server-tls"` |  |
| argo-cd.server.ingress.ingressClassName | string | `""` |  |
| argo-cd.server.ingressGrpc.annotations."cert-manager.io/cluster-issuer" | string | `"aux-issuer"` |  |
| argo-cd.server.ingressGrpc.enabled | bool | `true` |  |
| argo-cd.server.ingressGrpc.extraTls[0].hosts[0] | string | `"grpc.argocd.deepcypher.me"` |  |
| argo-cd.server.ingressGrpc.extraTls[0].secretName | string | `"argocd-server-grpc-tls"` |  |
| argo-cd.server.ingressGrpc.ingressClassName | string | `""` |  |
| argo-cd.server.resources.limits.memory | string | `"256Mi"` |  |
| argo-cd.server.resources.requests.cpu | string | `"500m"` |  |

