# auth

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://gitlab.com/api/v4/projects/55284972/packages/helm/stable | grocy | 0.2.3 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| grocy.barcode.ingress.annotations."cert-manager.io/cluster-issuer" | string | `"aux-issuer"` |  |
| grocy.barcode.ingress.annotations."traefik.ingress.kubernetes.io/router.middlewares" | string | `"traefik-headers@kubernetescrd"` |  |
| grocy.barcode.ingress.className | string | `"traefik"` |  |
| grocy.barcode.ingress.enabled | bool | `true` |  |
| grocy.barcode.ingress.hosts[0].host | string | `"barcode.deepcypher.me"` |  |
| grocy.barcode.ingress.hosts[0].paths[0].path | string | `"/"` |  |
| grocy.barcode.ingress.hosts[0].paths[0].pathType | string | `"Prefix"` |  |
| grocy.barcode.ingress.tls[0].hosts[0] | string | `"barcode.deepcypher.me"` |  |
| grocy.barcode.ingress.tls[0].secretName | string | `"barcode-tls"` |  |
| grocy.barcode.persistence.accessModes[0] | string | `"ReadWriteOnce"` |  |
| grocy.barcode.persistence.annotations | object | `{}` |  |
| grocy.barcode.persistence.enabled | bool | `false` |  |
| grocy.barcode.persistence.existingClaim | string | `""` |  |
| grocy.barcode.persistence.generatePVC | bool | `true` |  |
| grocy.barcode.persistence.labels | object | `{}` |  |
| grocy.barcode.persistence.size | string | `"8Gi"` |  |
| grocy.barcode.persistence.storageClass | string | `""` |  |
| grocy.ingress.annotations."cert-manager.io/cluster-issuer" | string | `"letsencrypt"` |  |
| grocy.ingress.className | string | `"traefik"` |  |
| grocy.ingress.enabled | bool | `true` |  |
| grocy.ingress.hosts[0].host | string | `"grocy.deepcypher.me"` |  |
| grocy.ingress.hosts[0].paths[0].path | string | `"/"` |  |
| grocy.ingress.hosts[0].paths[0].pathType | string | `"Prefix"` |  |
| grocy.ingress.tls[0].hosts[0] | string | `"grocy.deepcypher.me"` |  |
| grocy.ingress.tls[0].secretName | string | `"grocy-tls"` |  |
| grocy.netpol.enabled | bool | `true` |  |
| grocy.persistence.accessModes[0] | string | `"ReadWriteOnce"` |  |
| grocy.persistence.annotations | object | `{}` |  |
| grocy.persistence.enabled | bool | `true` |  |
| grocy.persistence.existingClaim | string | `""` |  |
| grocy.persistence.generatePVC | bool | `true` |  |
| grocy.persistence.labels | object | `{}` |  |
| grocy.persistence.size | string | `"100Gi"` |  |
| grocy.persistence.storageClass | string | `""` |  |

