# ghost

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://gitlab.com/api/v4/projects/55284972/packages/helm/stable | mysql-bkp(backupd) | 0.6.1 |
| https://gitlab.com/api/v4/projects/55284972/packages/helm/stable | ghost-bkp(backupd) | 0.6.1 |
| oci://registry-1.docker.io/bitnamicharts | ghost | 21.1.50 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| ghost-bkp.backup.copyMethod | string | `"Direct"` |  |
| ghost-bkp.backup.enabled | bool | `true` |  |
| ghost-bkp.backup.schedule | string | `"0 2 * * *"` |  |
| ghost-bkp.existingPVC | string | `"ghost"` |  |
| ghost-bkp.existingSecret | string | `"ghost-backupd-wasabi"` |  |
| ghost-bkp.persistence.generate | bool | `false` |  |
| ghost-bkp.persistence.size | string | `"50Gi"` |  |
| ghost-bkp.restore.enabled | bool | `false` |  |
| ghost-bkp.restore.schedule | string | `"0 3 * * *"` |  |
| ghost.allowEmptyPassword | bool | `false` |  |
| ghost.diagnosticMode.enabled | bool | `false` |  |
| ghost.existingSecret | string | `"ghost"` |  |
| ghost.extraEnvVarsSecret | string | `"ghost-extra"` |  |
| ghost.extraEnvVars[0].name | string | `"LOG_LEVEL"` |  |
| ghost.extraEnvVars[0].value | string | `"DEBUG"` |  |
| ghost.fullnameOverride | string | `"ghost"` |  |
| ghost.ghostBlogTitle | string | `"DeepCypher Blog"` |  |
| ghost.ghostEmail | string | `"noreply@smtp.deepcypher.me"` |  |
| ghost.ghostEnableHttps | bool | `false` |  |
| ghost.ghostHost | string | `"blog.deepcypher.me"` |  |
| ghost.ghostSkipInstall | bool | `false` |  |
| ghost.ghostUsername | string | `"Dr. George Onoufriou"` |  |
| ghost.image.debug | bool | `true` |  |
| ghost.ingress.annotations."cert-manager.io/cluster-issuer" | string | `"aux-issuer"` |  |
| ghost.ingress.annotations."gethomepage.dev/description" | string | `"Homelab blog."` |  |
| ghost.ingress.annotations."gethomepage.dev/enabled" | string | `"true"` |  |
| ghost.ingress.annotations."gethomepage.dev/group" | string | `"DeepCypher"` |  |
| ghost.ingress.annotations."gethomepage.dev/icon" | string | `"homepage.png"` |  |
| ghost.ingress.annotations."gethomepage.dev/name" | string | `"Ghost"` |  |
| ghost.ingress.annotations."traefik.ingress.kubernetes.io/router.middlewares" | string | `"ghost-headers@kubernetescrd"` |  |
| ghost.ingress.className | string | `"traefik"` |  |
| ghost.ingress.enabled | bool | `true` |  |
| ghost.ingress.hostname | string | `"blog.deepcypher.me"` |  |
| ghost.ingress.tls[0].hosts[0] | string | `"blog.deepcypher.me"` |  |
| ghost.ingress.tls[0].secretName | string | `"blog-cert"` |  |
| ghost.livenessProbe.enabled | bool | `false` |  |
| ghost.mysql.auth.existingSecret | string | `"mysql"` |  |
| ghost.mysql.auth.username | string | `"bn_ghost"` |  |
| ghost.mysql.primary.livenessProbe.enabled | bool | `false` |  |
| ghost.mysql.primary.persistence.size | string | `"50Gi"` |  |
| ghost.mysql.primary.readinessProbe.enabled | bool | `true` |  |
| ghost.mysql.primary.startupProbe.enabled | bool | `false` |  |
| ghost.mysql.primary.startupProbe.failureThreshold | int | `40` |  |
| ghost.mysql.primary.startupProbe.initialDelaySeconds | int | `500` |  |
| ghost.mysql.primary.startupProbe.periodSeconds | int | `15` |  |
| ghost.mysql.primary.startupProbe.successThreshold | int | `1` |  |
| ghost.mysql.primary.startupProbe.timeoutSeconds | int | `5` |  |
| ghost.mysql.resources.limits.ephemeral-storage | string | `"1Gi"` |  |
| ghost.mysql.resources.limits.memory | string | `"1Gi"` |  |
| ghost.mysql.resources.requests.cpu | string | `"750m"` |  |
| ghost.mysql.secondary.livenessProbe.enabled | bool | `false` |  |
| ghost.mysql.secondary.persistence.size | string | `"50Gi"` |  |
| ghost.mysql.secondary.readinessProbe.enabled | bool | `true` |  |
| ghost.mysql.secondary.startupProbe.enabled | bool | `false` |  |
| ghost.mysql.secondary.startupProbe.failureThreshold | int | `40` |  |
| ghost.mysql.secondary.startupProbe.initialDelaySeconds | int | `500` |  |
| ghost.mysql.secondary.startupProbe.periodSeconds | int | `15` |  |
| ghost.mysql.secondary.startupProbe.successThreshold | int | `1` |  |
| ghost.mysql.secondary.startupProbe.timeoutSeconds | int | `5` |  |
| ghost.persistence.enabled | bool | `true` |  |
| ghost.persistence.size | string | `"50Gi"` |  |
| ghost.readinessProbe.enabled | bool | `false` |  |
| ghost.replicaCount | int | `1` |  |
| ghost.resources.limits.ephemeral-storage | string | `"1Gi"` |  |
| ghost.resources.limits.memory | string | `"700Mi"` |  |
| ghost.resources.requests.cpu | string | `"500m"` |  |
| ghost.service.type | string | `"ClusterIP"` |  |
| ghost.smtpHost | string | `"in-v3.mailjet.com"` |  |
| ghost.smtpPort | int | `587` |  |
| ghost.startupProbe.enabled | bool | `false` |  |
| ghost.startupProbe.failureThreshold | int | `40` |  |
| ghost.startupProbe.initialDelaySeconds | int | `120` |  |
| ghost.startupProbe.periodSeconds | int | `15` |  |
| ghost.startupProbe.timeoutSeconds | int | `5` |  |
| ghost.volumePermissions.enabled | bool | `true` |  |
| mysql-bkp.backup.copyMethod | string | `"Direct"` |  |
| mysql-bkp.backup.enabled | bool | `true` |  |
| mysql-bkp.backup.schedule | string | `"0 2 * * *"` |  |
| mysql-bkp.existingPVC | string | `"data-ghost-mysql-0"` |  |
| mysql-bkp.existingSecret | string | `"mysql-backupd-wasabi"` |  |
| mysql-bkp.persistence.generate | bool | `false` |  |
| mysql-bkp.persistence.size | string | `"50Gi"` |  |
| mysql-bkp.restore.enabled | bool | `false` |  |
| mysql-bkp.restore.schedule | string | `"0 3 * * *"` |  |

