# knative-operator

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

Serverless event-driven applications

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://knative.github.io/operator | knative-operator | v1.17.3 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| knative-operator | object | `{}` |  |

