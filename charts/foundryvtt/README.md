# foundryvtt

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 11.315.0](https://img.shields.io/badge/AppVersion-11.315.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://gitlab.com/api/v4/projects/55284972/packages/helm/stable | foundry-bkp(backupd) | 0.7.0 |
| https://gitlab.com/api/v4/projects/55284972/packages/helm/stable | foundryvtt | 0.4.0 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| foundry-bkp.backup.copyMethod | string | `"Direct"` |  |
| foundry-bkp.backup.enabled | bool | `true` |  |
| foundry-bkp.backup.schedule | string | `"0 2 * * *"` |  |
| foundry-bkp.existingSecret | string | `"foundryvtt-backupd-wasabi"` |  |
| foundry-bkp.persistence.generate | bool | `true` |  |
| foundry-bkp.persistence.size | string | `"200Gi"` |  |
| foundry-bkp.restore.asOf | string | `"2024-08-31T06:00:00-00:00"` |  |
| foundry-bkp.restore.enabled | bool | `true` |  |
| foundry-bkp.restore.schedule | string | `"0 4 * * *"` |  |
| foundry-bkp.restore.trigger.manual | string | `"manual-2024-09-01T09-00-00"` |  |
| foundryvtt.env[0].name | string | `"FOUNDRY_IP_DISCOVERY"` |  |
| foundryvtt.env[0].value | string | `"false"` |  |
| foundryvtt.env[1].name | string | `"FOUNDRY_HOSTNAME"` |  |
| foundryvtt.env[1].value | string | `"dnd.deepcypher.me"` |  |
| foundryvtt.env[2].name | string | `"FOUNDRY_LOCAL_HOSTNAME"` |  |
| foundryvtt.env[2].value | string | `"dnd.deepcypher.me"` |  |
| foundryvtt.env[3].name | string | `"FOUNDRY_MINIFY_STATIC_FILES"` |  |
| foundryvtt.env[3].value | string | `"true"` |  |
| foundryvtt.env[4].name | string | `"FOUNDRY_UPNP"` |  |
| foundryvtt.env[4].value | string | `"false"` |  |
| foundryvtt.ingress.annotations."cert-manager.io/cluster-issuer" | string | `"aux-issuer"` |  |
| foundryvtt.ingress.annotations."traefik.ingress.kubernetes.io/router.middlewares" | string | `"traefik-headers@kubernetescrd"` |  |
| foundryvtt.ingress.className | string | `"traefik"` |  |
| foundryvtt.ingress.enabled | bool | `true` |  |
| foundryvtt.ingress.hosts[0].host | string | `"dnd.deepcypher.me"` |  |
| foundryvtt.ingress.hosts[0].paths[0].path | string | `"/"` |  |
| foundryvtt.ingress.hosts[0].paths[0].pathType | string | `"ImplementationSpecific"` |  |
| foundryvtt.ingress.tls[0].hosts[0] | string | `"dnd.deepcypher.me"` |  |
| foundryvtt.ingress.tls[0].secretName | string | `"dnd-tls"` |  |
| foundryvtt.netpol.enabled | bool | `true` |  |
| foundryvtt.persistence.enabled | bool | `false` |  |
| foundryvtt.persistence.existingClaim | string | `"foundryvtt-foundry-bkp"` |  |
| foundryvtt.persistence.size | string | `"200Gi"` |  |
| foundryvtt.securityContextEnabled | bool | `false` |  |
| foundryvtt.volumeMounts[0].mountPath | string | `"/data/"` |  |
| foundryvtt.volumeMounts[0].name | string | `"data"` |  |
| foundryvtt.volumes[0].name | string | `"data"` |  |
| foundryvtt.volumes[0].persistentVolumeClaim.claimName | string | `"foundryvtt-foundry-bkp"` |  |

