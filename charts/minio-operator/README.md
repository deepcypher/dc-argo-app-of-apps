# minio-operator

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 4](https://img.shields.io/badge/AppVersion-4-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://operator.min.io | operator | 6.0.4 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| operator.operator | object | `{}` |  |

