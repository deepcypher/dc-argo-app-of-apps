# traefik

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://traefik.github.io/charts | traefik | 31.1.1 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| traefik.autoscaling.enabled | bool | `true` |  |
| traefik.autoscaling.maxReplicas | int | `6` |  |
| traefik.autoscaling.metrics[0].resource.name | string | `"cpu"` |  |
| traefik.autoscaling.metrics[0].resource.target.averageUtilization | int | `80` |  |
| traefik.autoscaling.metrics[0].resource.target.type | string | `"Utilization"` |  |
| traefik.autoscaling.metrics[0].type | string | `"Resource"` |  |
| traefik.autoscaling.minReplicas | int | `2` |  |
| traefik.deployment.replicas | string | `nil` |  |
| traefik.fullnameOverride | string | `"traefik"` |  |
| traefik.ingressClass.name | string | `"traefik"` |  |
| traefik.logs.access.enabled | bool | `true` | To enable access logs |
| traefik.logs.access.fields.general.defaultmode | string | `"drop"` | Available modes: keep, drop, redact. |
| traefik.logs.access.fields.general.names | object | `{"ClientHost":"keep","DownstreamStatus":"keep","Duration":"keep","OriginContentSize":"keep","RequestAddr":"keep","RequestHost":"keep","RequestMethod":"keep","RequestPath":"keep","RouterName":"keep","StartUTC":"keep","request_User_Agent":"keep"}` | Names of the fields to limit. |
| traefik.logs.access.fields.headers.defaultmode | string | `"drop"` | Available modes: keep, drop, redact. |
| traefik.logs.access.fields.headers.names | object | `{"Authorization":"drop","Content-Type":"keep","User-Agent":"keep"}` | Names of the headers to limit. |
| traefik.logs.access.filters | object | `{}` | https://docs.traefik.io/observability/access-logs/#filtering |
| traefik.logs.access.format | string | `"json"` |  |
| traefik.logs.general.level | string | `"INFO"` | Alternative logging levels are DEBUG, PANIC, FATAL, ERROR, WARN, and INFO. |
| traefik.metrics.prometheus.addEntryPointsLabels | bool | `true` |  |
| traefik.metrics.prometheus.addRoutersLabels | bool | `true` |  |
| traefik.metrics.prometheus.addServicesLabels | bool | `true` |  |
| traefik.metrics.prometheus.disableAPICheck | bool | `true` |  |
| traefik.metrics.prometheus.entryPoint | string | `"metrics"` | Entry point used to expose metrics. |
| traefik.metrics.prometheus.service | object | `{"annotations":{},"enabled":false,"labels":{}}` | enable optional CRDs for Prometheus Operator  Create a dedicated metrics service for use with ServiceMonitor |
| traefik.metrics.prometheus.serviceMonitor.honorLabels | bool | `true` |  |
| traefik.metrics.prometheus.serviceMonitor.interval | string | `"30s"` |  |
| traefik.metrics.prometheus.serviceMonitor.jobLabel | string | `"traefik"` |  |
| traefik.metrics.prometheus.serviceMonitor.metricRelabelings[0].action | string | `"drop"` |  |
| traefik.metrics.prometheus.serviceMonitor.metricRelabelings[0].regex | string | `"^fluentd_output_status_buffer_(oldest|newest)_.+"` |  |
| traefik.metrics.prometheus.serviceMonitor.metricRelabelings[0].replacement | string | `"$1"` |  |
| traefik.metrics.prometheus.serviceMonitor.metricRelabelings[0].separator | string | `";"` |  |
| traefik.metrics.prometheus.serviceMonitor.metricRelabelings[0].sourceLabels[0] | string | `"__name__"` |  |
| traefik.metrics.prometheus.serviceMonitor.relabelings[0].action | string | `"replace"` |  |
| traefik.metrics.prometheus.serviceMonitor.relabelings[0].regex | string | `"^(.*)$"` |  |
| traefik.metrics.prometheus.serviceMonitor.relabelings[0].replacement | string | `"$1"` |  |
| traefik.metrics.prometheus.serviceMonitor.relabelings[0].separator | string | `";"` |  |
| traefik.metrics.prometheus.serviceMonitor.relabelings[0].sourceLabels[0] | string | `"__meta_kubernetes_pod_node_name"` |  |
| traefik.metrics.prometheus.serviceMonitor.relabelings[0].targetLabel | string | `"nodename"` |  |
| traefik.ports.web.nodePort | int | `30080` |  |
| traefik.ports.web.redirectTo.port | string | `"websecure"` |  |
| traefik.ports.websecure.nodePort | int | `30443` |  |
| traefik.ports.websecure.tls.enabled | bool | `true` |  |
| traefik.providers.kubernetesIngress.publishedService.enabled | bool | `true` |  |
| traefik.resources.limits.memory | string | `"150Mi"` |  |
| traefik.resources.requests.cpu | string | `"100m"` |  |
| traefik.resources.requests.memory | string | `"150Mi"` |  |
| traefik.service.enabled | bool | `true` |  |
| traefik.service.labels.l2 | string | `"active"` |  |
| traefik.service.type | string | `"LoadBalancer"` |  |

