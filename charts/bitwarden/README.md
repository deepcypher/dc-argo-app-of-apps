# bitwarden

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.bitwarden.com/ | bitwarden(self-host) | 2024.12.1 |
| oci://registry-1.docker.io/bitnamicharts | postgresql-ha | 14.3.10 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| bitwarden.cloudRegion | string | `"US"` |  |
| bitwarden.component.volume.logs.enabled | bool | `true` |  |
| bitwarden.database.enabled | bool | `false` |  |
| bitwarden.enableCloudCommunication | bool | `true` |  |
| bitwarden.general.domain | string | `"bitwarden.deepcypher.me"` |  |
| bitwarden.general.email.replyToEmail | string | `"noreply@smtp.deepcypher.me"` |  |
| bitwarden.general.email.smtpHost | string | `"in-v3.mailjet.com"` |  |
| bitwarden.general.email.smtpPort | int | `587` |  |
| bitwarden.general.email.smtpSsl | bool | `true` |  |
| bitwarden.general.ingress.annotations."cert-manager.io/cluster-issuer" | string | `"aux-issuer"` |  |
| bitwarden.general.ingress.annotations."traefik.ingress.kubernetes.io/router.middlewares" | string | `"traefik-headers@kubernetescrd"` |  |
| bitwarden.general.ingress.cert.tls.name | string | `"bitwarden.deepcypher.me-tls"` |  |
| bitwarden.general.ingress.className | string | `"traefik"` |  |
| bitwarden.general.ingress.enabled | bool | `true` |  |
| bitwarden.secrets.secretName | string | `"bitwarden"` |  |
| postgresql-ha.enabled | bool | `true` |  |
| postgresql-ha.fullnameOverride | string | `"postgres"` |  |
| postgresql-ha.persistence.enabled | bool | `true` |  |
| postgresql-ha.persistence.size | string | `"8Gi"` |  |
| postgresql-ha.pgpool.adminUsername | string | `"admin"` |  |
| postgresql-ha.pgpool.childMaxConnections | int | `1` |  |
| postgresql-ha.pgpool.containerPorts.postgresql | int | `5432` |  |
| postgresql-ha.pgpool.existingSecret | string | `"pgpool"` |  |
| postgresql-ha.pgpool.numInitChildren | int | `256` |  |
| postgresql-ha.pgpool.replicaCount | int | `1` |  |
| postgresql-ha.pgpool.resources.limits.ephemeral-storage | string | `"2Gi"` |  |
| postgresql-ha.pgpool.resources.limits.memory | string | `"2Gi"` |  |
| postgresql-ha.pgpool.resources.requests.cpu | string | `"600m"` |  |
| postgresql-ha.postgresql.containerPorts.postgresql | int | `5432` |  |
| postgresql-ha.postgresql.database | string | `"postgres"` |  |
| postgresql-ha.postgresql.existingSecret | string | `"postgresql"` |  |
| postgresql-ha.postgresql.replicaCount | int | `1` |  |
| postgresql-ha.postgresql.resources.limits.memory | string | `"2Gi"` |  |
| postgresql-ha.postgresql.resources.requests.cpu | string | `"600m"` |  |
| postgresql-ha.postgresql.username | string | `"postgres"` |  |

