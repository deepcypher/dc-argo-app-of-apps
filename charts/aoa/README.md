# dc-aoa

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.1.0](https://img.shields.io/badge/AppVersion-0.1.0-informational?style=flat-square)

Deep Cypher Argo App-of-Apps Helm Deployment

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| cert.email | string | `"you@example.com"` |  |
| collaboraHelm.branch | bool | `false` |  |
| exmongo.persistence.size | string | `"10Gi"` |  |
| global.branch | string | `"master"` |  |
| hass.branch | bool | `false` |  |
| hass.configClass | string | `"home-assistant"` |  |
| hass.configSize | string | `"15Gi"` |  |
| hass.ingress.enable | bool | `false` |  |
| hass.persist | bool | `false` |  |
| jellyfinHelm.branch | bool | `false` |  |
| jellyfinHelm.configClass | string | `"jellyfin"` |  |
| jellyfinHelm.configSize | string | `"15Gi"` |  |
| jellyfinHelm.ingress.enable | bool | `false` |  |
| jellyfinHelm.mediaClass | string | `"jellyfin"` |  |
| jellyfinHelm.mediaSize | string | `"2Ti"` |  |
| jellyfinHelm.persist | bool | `false` |  |

