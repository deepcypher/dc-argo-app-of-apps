# gitea

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://dl.gitea.com/charts/ | gitea | 10.6.0 |
| https://gitlab.com/api/v4/projects/55284972/packages/helm/stable | gitea-bkp(backupd) | 0.6.1 |
| https://gitlab.com/api/v4/projects/55284972/packages/helm/stable | postgresql-bkp(backupd) | 0.6.1 |
| oci://registry-1.docker.io/bitnamicharts | postgresql-ha | 14.3.10 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| gitea-bkp.backup.copyMethod | string | `"Direct"` |  |
| gitea-bkp.backup.enabled | bool | `true` |  |
| gitea-bkp.backup.schedule | string | `"0 2 * * *"` |  |
| gitea-bkp.enabled | bool | `true` |  |
| gitea-bkp.existingPVC | string | `"gitea-shared-storage"` |  |
| gitea-bkp.existingSecret | string | `"gitea-backupd"` |  |
| gitea-bkp.persistence.generate | bool | `false` |  |
| gitea-bkp.persistence.size | string | `"100Gi"` |  |
| gitea-bkp.restore.enabled | bool | `false` |  |
| gitea-bkp.restore.schedule | string | `"0 4 * * *"` |  |
| gitea.enabled | bool | `true` |  |
| gitea.gitea.additionalConfigFromEnvs[0].name | string | `"GITEA__DATABASE__PASSWD"` |  |
| gitea.gitea.additionalConfigFromEnvs[0].valueFrom.secretKeyRef.key | string | `"password"` |  |
| gitea.gitea.additionalConfigFromEnvs[0].valueFrom.secretKeyRef.name | string | `"postgresql"` |  |
| gitea.gitea.additionalConfigSources[0].secret.secretName | string | `"smtp"` |  |
| gitea.gitea.admin.existingSecret | string | `"gitea-admin-secret"` |  |
| gitea.gitea.config.APP_NAME | string | `"Gitea: A private Git Service"` |  |
| gitea.gitea.config.database.DB_TYPE | string | `"postgres"` |  |
| gitea.gitea.config.database.HOST | string | `"postgres-pgpool"` |  |
| gitea.gitea.config.database.NAME | string | `"postgres"` |  |
| gitea.gitea.config.database.SSL_MODE | string | `"disable"` |  |
| gitea.gitea.config.database.USER | string | `"postgres"` |  |
| gitea.gitea.config.metrics.ENABLED | bool | `true` |  |
| gitea.gitea.config.server.LFS_START_SERVER | bool | `true` |  |
| gitea.gitea.config.service.DISABLE_REGISTRATION | bool | `true` |  |
| gitea.ingress.annotations."cert-manager.io/cluster-issuer" | string | `"letsencrypt"` |  |
| gitea.ingress.annotations."traefik.ingress.kubernetes.io/router.middlewares" | string | `"traefik-headers@kubernetescrd"` |  |
| gitea.ingress.enabled | bool | `true` |  |
| gitea.ingress.hosts[0].host | string | `"git.deepcypher.me"` |  |
| gitea.ingress.hosts[0].paths[0].path | string | `"/"` |  |
| gitea.ingress.hosts[0].paths[0].pathType | string | `"Prefix"` |  |
| gitea.ingress.tls[0].hosts[0] | string | `"git.deepcypher.me"` |  |
| gitea.ingress.tls[0].secretName | string | `"git.deepcypher.me-tls"` |  |
| gitea.persistence.claimName | string | `"gitea-shared-storage"` |  |
| gitea.persistence.create | bool | `true` |  |
| gitea.persistence.enabled | bool | `true` |  |
| gitea.persistence.size | string | `"100Gi"` |  |
| gitea.postgresql-ha.enabled | bool | `false` |  |
| gitea.postgresql.enabled | bool | `false` |  |
| postgresql-bkp.backup.copyMethod | string | `"Direct"` |  |
| postgresql-bkp.backup.enabled | bool | `true` |  |
| postgresql-bkp.backup.schedule | string | `"0 2 * * *"` |  |
| postgresql-bkp.enabled | bool | `true` |  |
| postgresql-bkp.existingPVC | string | `"data-postgres-postgresql-0"` |  |
| postgresql-bkp.existingSecret | string | `"postgresql-backupd"` |  |
| postgresql-bkp.persistence.generate | bool | `false` |  |
| postgresql-bkp.persistence.size | string | `"8Gi"` |  |
| postgresql-bkp.restore.enabled | bool | `false` |  |
| postgresql-bkp.restore.schedule | string | `"0 4 * * *"` |  |
| postgresql-ha.enabled | bool | `true` |  |
| postgresql-ha.fullnameOverride | string | `"postgres"` |  |
| postgresql-ha.image.registry | string | `"docker.io"` |  |
| postgresql-ha.image.repository | string | `"bitnami/postgresql-repmgr"` |  |
| postgresql-ha.image.tag | string | `"16.2.0-debian-11-r17"` |  |
| postgresql-ha.persistence.enabled | bool | `true` |  |
| postgresql-ha.persistence.size | string | `"8Gi"` |  |
| postgresql-ha.pgpool.adminUsername | string | `"admin"` |  |
| postgresql-ha.pgpool.containerPorts.postgresql | int | `5432` |  |
| postgresql-ha.pgpool.existingSecret | string | `"pgpool"` |  |
| postgresql-ha.pgpool.replicaCount | int | `1` |  |
| postgresql-ha.postgresql.containerPorts.postgresql | int | `5432` |  |
| postgresql-ha.postgresql.database | string | `"postgres"` |  |
| postgresql-ha.postgresql.existingSecret | string | `"postgresql"` |  |
| postgresql-ha.postgresql.replicaCount | int | `1` |  |
| postgresql-ha.postgresql.upgradeRepmgrExtension | bool | `true` |  |
| postgresql-ha.postgresql.username | string | `"postgres"` |  |

