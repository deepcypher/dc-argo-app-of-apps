# descheduler

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

Descheduler for kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://kubernetes-sigs.github.io/descheduler/ | descheduler | 0.32.2 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| descheduler.deschedulerPolicy.profiles[0].name | string | `"default"` |  |
| descheduler.deschedulerPolicy.profiles[0].pluginConfig[0].args.evictLocalStoragePods | bool | `true` |  |
| descheduler.deschedulerPolicy.profiles[0].pluginConfig[0].args.ignorePvcPods | bool | `true` |  |
| descheduler.deschedulerPolicy.profiles[0].pluginConfig[0].name | string | `"DefaultEvictor"` |  |
| descheduler.deschedulerPolicy.profiles[0].pluginConfig[1].name | string | `"RemoveDuplicates"` |  |
| descheduler.deschedulerPolicy.profiles[0].pluginConfig[2].args.includingInitContainers | bool | `true` |  |
| descheduler.deschedulerPolicy.profiles[0].pluginConfig[2].args.podRestartThreshold | int | `100` |  |
| descheduler.deschedulerPolicy.profiles[0].pluginConfig[2].name | string | `"RemovePodsHavingTooManyRestarts"` |  |
| descheduler.deschedulerPolicy.profiles[0].pluginConfig[3].args.nodeAffinityType[0] | string | `"requiredDuringSchedulingIgnoredDuringExecution"` |  |
| descheduler.deschedulerPolicy.profiles[0].pluginConfig[3].name | string | `"RemovePodsViolatingNodeAffinity"` |  |
| descheduler.deschedulerPolicy.profiles[0].pluginConfig[4].name | string | `"RemovePodsViolatingNodeTaints"` |  |
| descheduler.deschedulerPolicy.profiles[0].pluginConfig[5].name | string | `"RemovePodsViolatingInterPodAntiAffinity"` |  |
| descheduler.deschedulerPolicy.profiles[0].pluginConfig[6].name | string | `"RemovePodsViolatingTopologySpreadConstraint"` |  |
| descheduler.deschedulerPolicy.profiles[0].pluginConfig[7].args.targetThresholds.cpu | int | `80` |  |
| descheduler.deschedulerPolicy.profiles[0].pluginConfig[7].args.targetThresholds.memory | int | `80` |  |
| descheduler.deschedulerPolicy.profiles[0].pluginConfig[7].args.targetThresholds.pods | int | `50` |  |
| descheduler.deschedulerPolicy.profiles[0].pluginConfig[7].args.thresholds.cpu | int | `60` |  |
| descheduler.deschedulerPolicy.profiles[0].pluginConfig[7].args.thresholds.memory | int | `60` |  |
| descheduler.deschedulerPolicy.profiles[0].pluginConfig[7].args.thresholds.pods | int | `20` |  |
| descheduler.deschedulerPolicy.profiles[0].pluginConfig[7].name | string | `"LowNodeUtilization"` |  |
| descheduler.deschedulerPolicy.profiles[0].plugins.balance.enabled[0] | string | `"RemoveDuplicates"` |  |
| descheduler.deschedulerPolicy.profiles[0].plugins.balance.enabled[1] | string | `"RemovePodsViolatingTopologySpreadConstraint"` |  |
| descheduler.deschedulerPolicy.profiles[0].plugins.balance.enabled[2] | string | `"LowNodeUtilization"` |  |
| descheduler.deschedulerPolicy.profiles[0].plugins.deschedule.enabled[0] | string | `"RemovePodsHavingTooManyRestarts"` |  |
| descheduler.deschedulerPolicy.profiles[0].plugins.deschedule.enabled[1] | string | `"RemovePodsViolatingNodeTaints"` |  |
| descheduler.deschedulerPolicy.profiles[0].plugins.deschedule.enabled[2] | string | `"RemovePodsViolatingNodeAffinity"` |  |
| descheduler.deschedulerPolicy.profiles[0].plugins.deschedule.enabled[3] | string | `"RemovePodsViolatingInterPodAntiAffinity"` |  |

