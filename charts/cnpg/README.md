# cnpg

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

Cloud native postgresql operator

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://cloudnative-pg.github.io/charts | cloudnative-pg | 0.22.1 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| cloudnative-pg.crds.create | bool | `true` |  |
| cloudnative-pg.monitoring.podMonitorEnabled | bool | `true` |  |
| cloudnative-pg.replicaCount | int | `3` |  |

